<?php

/**
 * @author 
 *
 * Copy this file to wp-config-local.php to overwrite the live settings
 */
/* Database configurations */
define('DB_NAME', 'wp_wordpress_101_alessandro_castellani');                   // 1/2 Datenbank angeben
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');

/* Website configurations */
define('WP_HOME', 'http://wordpress-101-alessandro-castellani.lab');         // 2/2 URL angeben (siehe etc/hosts, /usr/local/etc/httpd/extra/httpd-vhosts.conf)
define('WP_SITEURL', WP_HOME);

/* Debug configurations */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true); //Enable Debug logging to the /wp-content/debug.log file
define('WP_DEBUG_DISPLAY', false); //Disable display of errors and warnings

/* No index for website */
define('NOINDEX', true);
