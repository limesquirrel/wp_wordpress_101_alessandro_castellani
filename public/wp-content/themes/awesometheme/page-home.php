<?php get_header(); ?>

<div class="row">

    <div class="col-xs-12">

        <!--
// ====================================================== //
// ================= Bootstrap Carousel ================= //
// ====================================================== // -->
        <div id="awesome-carousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <!-- / carousel -->

                <?php
                // ====================================================== //
                // ==================== LETZTER POST ==================== //
                // ====================================================== //
                // var declaration

                // array for category filter
                $args_cat = array(
                    // include as a String
                    'include' => '13,14,15'
                );

                $categories = get_categories($args_cat);

                $count = 0;
                $bullets = '';

                // $categoriy, for each var (Umgekehrt als in java foreach loop)
                foreach ($categories as $category) :

                    $args = array(
                        'type' => 'post',
                        'posts_per_page' => 1,
                        'category__in' => $category->term_id,
                        'category__not_in' => array(1, 16)
                    );

                    $lastBlog = new WP_Query($args);

                    // The WordPress Post Loop (andere Syntax)
                    if ($lastBlog->have_posts()) :

                        while ($lastBlog->have_posts()) : $lastBlog->the_post(); ?>

                            <!-- div class="col-xs-12 col-sm-4">
                            <?php //get_template_part('content', 'featured'); 
                            ?>
                        </div -->

                            <div class="item <?php if ($count == 0) : echo 'active';
                                                endif; ?>">
                                <?php the_post_thumbnail('full'); ?>
                                <div class="carousel-caption">
                                    <?php the_title(sprintf('<h1 class="entry-title"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>

                                    <small><?php the_category(' '); ?></small>
                                </div>
                            </div>

                            <?php $bullets .= '<li data-target="#awesome-carousel" data-slide-to=" ' . $count . ' " class="'; ?>
                            <?php if ($count == 0) : $bullets .= 'active'; endif; ?> 
                            <?php $bullets .= ' "></li>'; ?>

                <?php endwhile;

                    endif; // / if WP LOOP

                    wp_reset_postdata();

                    $count++;
                endforeach;

                ?>

                <!-- carousel -->

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php echo $bullets; ?>
                </ol>

            </div> <!-- / .carousel-inner -->

            <!-- Controls -->
            <a class="left carousel-control" href="#awesome-carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </a>
            <a class="right carousel-control" href="#awesome-carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </a>
        </div><!-- / .awesome-carousel -->
    </div> <!-- / col-xs-12 -->
</div> <!-- / .row -->

<div class="row">

    <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
    <div class="col-xs-12 col-sm-8">

        <!-- h1>This the index content</h1 -->
        <?php
        // ====================================================== //
        // ============== ALTER INDEX WP POST LOOP ============== //
        // ====================================================== //
        // The WordPress Post Loop
        if (have_posts()) :

            while (have_posts()) : the_post(); ?>

                <?php get_template_part('content', get_post_format()); ?>

        <?php endwhile;

        endif; // / if WP LOOP

        // ====================================================== //
        // ============ VOR UND VORVOR LETZTEN POSTS ============ //
        // ====================================================== //
        // PRINT OTHER 2 POSTS NOT THE FIRST ONE
        /*
// var declaration
$args = array(
'type' => 'post',
'posts_per_page' => 2,
'offset' => '1'
);
//$lastBlog = new WP_Query('type=post&posts_per_page=2&offset=1');
$lastBlog = new WP_Query($args);


// The WordPress Post Loop (andere Syntax)
if( $lastBlog->have_posts() ):
while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
<?php get_template_part('content', get_post_format()); ?>
<?php endwhile;
endif; // / if WP LOOP
wp_reset_postdata();
*/
        ?>
        <!-- <hr />
<hr /> -->
        <?php
        /*
// ====================================================== //
// ================ PRINT ONLY TUTORIALS ================ //
// ====================================================== //
// var declaration
// -1 means unlimited posts shown
//$lastBlog = new WP_Query('type=post&posts_per_page=-1&cat=13');
$lastBlog = new WP_Query('type=post&posts_per_page=-1&category_name=tutorials');


// The WordPress Post Loop (andere Syntax)
if( $lastBlog->have_posts() ):
while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
<?php get_template_part('content', get_post_format()); ?>
<?php endwhile;
endif; // / if WP LOOP
wp_reset_postdata();
*/
        ?>
    </div>
    <div class="col-xs-12 col-sm-4">

        <?php get_sidebar(); ?>

    </div>
</div> <!-- / .row -->

<?php get_footer(); ?>