<!-- Beitrags-Seite -->
<?php get_header(); ?>
<div class="row">
    <div class="col-xs-12 col-sm-8">

        <?php
        // WP Post Loop
        if (have_posts()) :
            while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php the_title('<h1 class="entry-title">','</h1>' ); ?>

                    <?php if (has_post_thumbnail()) : ?>
                        <div class="pull-right"><?php the_post_thumbnail('thumbnail'); ?></div>
                    <?php endif; ?>

                    <!-- (' ') Formatierung, keine Liste mehr -->
                    <small>
                        <?php 

                           

                         /*    $terms_list = wp_get_post_terms( $post->ID, 'field' ); 
                            
                            $i = 0;
                            foreach( $terms_list as $term ){ 
                                $i++;
                                if($i > 1){
                                    echo ', ';
                                }
                                echo $term->name.' ';
                            } */

                            echo awesome_get_terms( $post->ID, 'field' );
                        ?> 

                        || 

                        <?php 
                          /*   $terms_list2 = wp_get_post_terms( $post->ID, 'software' ); 
                            
                            $ii = 0;
                            foreach( $terms_list2 as $term ){ 
                                $ii++;
                                if($ii > 1){
                                    echo ', ';
                                }
                                echo $term->name.' ';
                            }
                         */

                        echo awesome_get_terms( $post->ID, 'software' );
                        
                        ?> 

                        <?php 
                            if( current_user_can('manage_options') ){   // boolean, if true...
                                echo '|| ';
                                edit_post_link(); 
                            }
                        ?> 


                    </small>
                    <?php the_content(); ?>

                    <hr />
                    <div class="row">    
                        <!-- Reihenfolge beachten -->
                        <div class="col-xs-6 text-left" style="display: inline;">
                            <!-- post Singular -->
                            <?php previous_post_link(); ?>
                        </div>
                        <div class="col-xs-3 text-right" style="display: inline;">
                            <!-- post Singular -->
                            <?php next_post_link(); ?>
                        </div>

                    </div> <!-- / .row -->

                </article>

        <?php endwhile;
        endif;
        ?>
    </div> <!-- / .col-xs-12 -->
    <div class="col-xs-12 col-sm-4">
        <?php get_sidebar(); ?>
    </div> <!-- / .col-xs-12 -->
</div> <!-- / .row -->
<?php get_footer(); ?>