<!-- Beitrags-Seite -->
<?php get_header(); ?>
<div class="row">
    <div class="col-xs-12 col-sm-8">

        <?php
        // WP Post Loop
        if (have_posts()) :
            while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php the_title('<h1 class="entry-title">','</h1>' ); ?>

                    <?php if (has_post_thumbnail()) : ?>
                        <div class="pull-right"><?php the_post_thumbnail('thumbnail'); ?></div>
                    <?php endif; ?>

                    <!-- (' ') Formatierung, keine Liste mehr -->
                    <small><?php the_category(' '); ?> || <?php the_tags(); ?> || <?php edit_post_link(); ?> </small>
                    <?php the_content(); ?>

                    <hr />
                    <div class="row">    
                        <!-- Reihenfolge beachten -->
                        <div class="col-xs-6 text-left" style="display: inline;">
                            <!-- post Singular -->
                            <?php previous_post_link(); ?>
                        </div>
                        <div class="col-xs-3 text-right" style="display: inline;">
                            <!-- post Singular -->
                            <?php next_post_link(); ?>
                        </div>

                    </div> <!-- / .row -->

                    <?php 
                    if(comments_open() ) {
                         comments_template(); 
                        } else { 
                        echo '<h5 class="text-center">Sorry, Comments are closed!</h5>' ;
                    }
                    ?>
                </article>

        <?php endwhile;
        endif;
        ?>
    </div> <!-- / .col-xs-12 -->
    <div class="col-xs-12 col-sm-4">
        <?php get_sidebar(); ?>
    </div> <!-- / .col-xs-12 -->
</div> <!-- / .row -->
<?php get_footer(); ?>