<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">  
    <title>Awesome Theme</title>
    <?php wp_head(); ?>
</head>

<!-- Abfrage ob man in der Home-page ist 
Zum Verständnis: Die Home-Page ist die Page, in der der wp-loop abläuft. Nicht in der Home-page, sondern in der Blog-Page
-->
<?php
    if(is_front_page() ):
        // Deklaration php var $awesome_classes
        $awesome_classes = array('awesome-class', 'my-class');
    else:
        $awesome_classes = array('no-awesome-class');
    endif;
?>

<!-- Initialisierung php var $awesome_classes -->
<body 
    <?php body_class( $awesome_classes); ?>
>

<!-- Menu header navigation -->
<!-- PHP Syntax -->
<?php wp_nav_menu(array(
    'theme_location' => 'primary'
)); 
?>

<!-- header, add_theme_support function 
echo, "calls" attributes. PHP Syntax.  
get_custom_header ist ein array, mit attributen wie width / height. Hier werden sie ausgelesen. PHP Syntax.
-->
<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />