<!-- Für Post Format: Standard -- aus der index.php ausgeschnitten -->
<p><?php echo '<br /> THIS IS THE FORMAT: ' . get_post_format(); ?></p>
<h3>STANDART POST: <?php the_title(); ?></h3>
<div class="thumbnail-img"><?php the_post_thumbnail('thumbnail'); ?></div>
<small>Posted on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?>, in <?php the_category(); ?></small>
<p><?php the_content(); ?></p>
<hr />