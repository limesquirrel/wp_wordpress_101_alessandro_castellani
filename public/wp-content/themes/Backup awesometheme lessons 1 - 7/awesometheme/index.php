<?php get_header(); ?>

<!-- h1>This the index content</h1 -->
<?php 
    // The WordPress Post Loop
    if(have_posts() ){
        while( have_posts() ): the_post();  ?>

            <?php get_template_part('content', get_post_format()); ?>

        <?php endwhile;
    }
?>
<?php get_footer(); ?>