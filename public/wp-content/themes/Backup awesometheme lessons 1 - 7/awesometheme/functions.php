<?php

function awesome_script_enqueue() {
    // hooks, wp_
    // CSS
    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/awesome.css', array(), '1.0.0', 'all');
    // JS
    // script, singular
    // true, print in footer, false (default) print in header
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/awesome.js', true);
}
// calling function, action
// first hook wp_enqueue_scripts as STRING!
// scripts, plural
add_action('wp_enqueue_scripts', 'awesome_script_enqueue');

// Menus
function awesome_theme_setup() {
    add_theme_support('menus');

    // Menü aktivieren/registering
    // menu, singular
    // 2. String, eigene Beschreibung
    // Header Menu
    register_nav_menu('primary', 'Primary Header Navigation');
    // Footer Menu
    register_nav_menu('secondary', 'Footer Navigation');
}
// calling function menus
add_action('init', 'awesome_theme_setup');

// hook
// Funktioniert ohne add_action innerhalb der functions.php
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');

add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

