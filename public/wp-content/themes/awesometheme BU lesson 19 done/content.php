<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="entry-header">
    <!-- Für Post Format: Standard -- aus der index.php ausgeschnitten -->
    <p><?php //echo '<br /> THIS IS THE FORMAT: ' . get_post_format(); ?></p>

    <!-- STANDART POST: --><?php the_title(sprintf( '<h1 class="entry-title"><a href="%s">' , esc_url( get_permalink() ) ) , '</a></h1>' ); ?>
    <small>Posted on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?>, in category <?php the_category(); ?></small>
</header>

<div class="row">
    <?php if( has_post_thumbnail() ): ?>
    <!-- left column -->
    <div class="col-xs-12 col-sm-4">
        <div class="thumbnail"><?php the_post_thumbnail('medium'); ?></div>
    </div>
    <!-- right column -->
    <div class="col-xs-12 col-sm-8">
        <p><?php the_content(); ?></p>
    </div>

    <?php else: ?>

         <!-- full size 12 column -->
        <div class="col-xs-12">
            <p><?php the_content(); ?></p>
        </div>
    <?php endif; ?>
</div> 


<hr />

</article>
