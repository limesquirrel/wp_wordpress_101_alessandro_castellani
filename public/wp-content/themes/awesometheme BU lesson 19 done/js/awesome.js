// js functions

// jQuery
// ensurance, $ wont conflict with other frameworks
//$.noConflict();

jQuery(document).ready(function() {
//$(document).ready(function() {

    // Evente Listener on a-tag
    jQuery(document).on('click','.open-search > a', function(e){
        // no back to top jump
        e.preventDefault();
        //console.log('clicked...');

        // toggle down menu
        jQuery('.search-form-container').slideToggle(300);

    });

});