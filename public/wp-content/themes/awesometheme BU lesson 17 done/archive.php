<?php get_header(); ?>

<div class="row">

    <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
    <div class="col-xs-12 col-sm-8">

        <div class="row text-center">

            <!-- h1>This the index content</h1 -->
            <?php


            // The WordPress Post Loop
            if (have_posts()) : ?>

                <header class="page-header">
                    <?php 
                        the_archive_title('<h1 class="page-title">', '</h1>');
                        the_archive_description('<div class="taxonomy-description">', '</div>');
                    ?>
                </header>    

                <?php 
                // (The WordPress Post Loop)
                while (have_posts()) : the_post(); ?>

                    <?php get_template_part('content', 'archive'); ?>

                <?php  endwhile; ?><!-- / endwhile The WordPress Post Loop -->

                <div class="col-xs-12 text-center">
                   <?php the_post_navigation(); ?>
                </div>

            <?php endif; ?>
            
            <!-- / WP Post Loop -->

        </div><!-- / .row .text-center -->

    </div>
    <div class="col-xs-12 col-sm-4">

        <?php get_sidebar(); ?>

    </div>
</div> <!-- / .row -->

<?php get_footer(); ?>