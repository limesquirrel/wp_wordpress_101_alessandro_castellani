<?php get_header(); ?>

<div class="row">

    <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
    <div class="col-xs-12 col-sm-8">

        <div class="row text-center">

            <!-- h1>This the index content</h1 -->
            <?php

            // IF Syntax, erweiterte: Wenn page ID (paged) ist 0, dann ...
            $currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
            //echo $currentPage;
            $args = array(
                'posts_per_page' => 3,
                'paged' => $currentPage
            );
            query_posts($args);


            

            // The WordPress Post Loop
            if (have_posts()) : $i = 0;

                // (The WordPress Post Loop)
                while (have_posts()) : the_post(); ?>

                    <?php //get_template_part('content', get_post_format()); 
                    ?>

                    <!--
                // ====================================================== //
                // ====================== ALL SIZES ===================== //
                // ====================================================== //
                -->
                    <?php 
                        if ($i == 0) : $column = 12; 
                        elseif ($i > 0 && $i <= 2) : $column = 6; $class = ' second-row-padding';
                        elseif ($i > 2) : $column = 4; $class= ' third-row-padding';
                        endif;
                    ?>

                    <div class="col-xs-<?php echo $column; echo $class; ?>">
                            <?php if (has_post_thumbnail()) : 
                                $urlImg = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));
                            endif; ?>
                        <div class="blog-element" style="background-image: url(<?php echo $urlImg; ?>);">
                            
                            <?php the_title(sprintf('<h1 class="entry-title"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>

                            <small><?php the_category(' '); ?></small>
                        </div> <!-- / .blog-element -->        
                    </div> <!-- / .col-xs-__ -->

                <?php $i++; endwhile; ?><!-- / endwhile The WordPress Post Loop -->

                <div class="col-xs-6 text-left">
                    <!-- posts Plural -->
                    <?php next_posts_link('<< Older Posts'); ?>
                </div>
                <div class="col-xs-6 text-right">
                    <!-- posts Plural -->
                    <?php previous_posts_link(' Newer Posts >>'); ?>
                </div>

            <?php 
                endif; 
                wp_reset_query();
            ?>
            
            <!-- / WP Post Loop -->

        </div><!-- / .row .text-center -->

    </div>
    <div class="col-xs-12 col-sm-4">

        <?php get_sidebar(); ?>

    </div>
</div> <!-- / .row -->

<?php get_footer(); ?>