<?php  
// ##################################################################### //
// #################### Collection of Walker Classes ################### //
// ##################################################################### //

/* Introduction

    wp_nav_menu()

    <div class="menu-container">
        <ul>                // start_lvl()
            <li><a><span>   // start_el()
                </a></span>
            </li>           // end_all()
        </ul>               // end_lvl()
    </div>
 */

// Deklaration
// Walker_Nav_Menu existiert, daher extends (Wie in Java)
class Walker_Nav_Primary extends Walker_Nav_Menu {

    // $output, $depth sind defaults
    function start_lvl( &$output, $depth ){   // ul
        // \t tab
        $intend = str_repeat("\t", $depth);
        // php if Abfrage
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        // \n new line
        $output .= "\n$intend<ul class=\"dropdown-menu$submenu depth_$depth\">\n";

    }
    
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){    // li a span
        
        // ====================================================== //
        // ======================== <li> ======================== //
        // ====================================================== //
        $intend = ($depth) ? str_repeat("\t",$depth) : '';

        $li_attributes = '';
        $class_names = $value = '';

        // checking classes of item
        // empty() php default function
        // Abfrage
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        // php array[], merge
        // $args ist ein default WP array, has_children ist ein default attribute
        // Abfrage, wenn es ein Untermenü gibt, dann mache dropdown-class
        $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
        // Abfrage, ist item current aktiv? Mit ODER Abfrage
        $classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
        // Concatenation
        $classes[] = 'menu-item-' . $item->ID;
        // Abfrage ist item sub-el von sub-el?
        if( $depth && $args->walker->has_children ){
            $classes[] = 'dropdown-submenu';
        }
        //  php merge function join()
        // class Abfrage, Loop mit Filter
        $class_names = join( ' ', apply_filters('nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        // print into WP
        $class_names = ' class="' . esc_attr($class_names) . '" ';

        // Definition of WP ID
        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        // Abfrage
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '" ' : '';

        // Print output, .= merge $output mit .= Inhalt
        $output .= $intend . '<li' . $id . $value . $class_names . $li_attributes . '>';

        // ====================================================== //
        // ======================= a span ======================= //
        // ====================================================== //
        // if Abfrage, is-not-empty
        $attributes = ! empty( $item->attr_title ) ? ' title=" ' . esc_attr($item->attr_title) . '" ' : '';
        $attributes .= ! empty( $item->target ) ? ' target=" ' . esc_attr($item->target) . '" ' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel=" ' . esc_attr($item->xfn) . '" ' : '';
        $attributes .= ! empty( $item->url ) ? ' href=" ' . esc_attr($item->url) . '" ' : '';

        $attributes .= ( $args->walker->has_children ) ? ' class="dropdown-toggle" data-toggle="dropdown" ' : '';

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        // caret down symbol
        $item_output .= ( $depth == 0 && $args->walker->has_children ) ? ' <b class="caret"></b></a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

    }
/* 
    function end_el(){      // closing li a span

    }
    function end_lvl(){     // closing ul

    }
*/
}