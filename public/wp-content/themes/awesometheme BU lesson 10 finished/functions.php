<?php
// ====================================================== //
// =================== Include Scripts ================== //
// ====================================================== //
function awesome_script_enqueue()
{
    // ---------- CSS --------- //
    // Bootstrap
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.4', 'all');
    // Eigenes style
    // hooks, wp_
    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/awesome.css', array(), '1.0.0', 'all');
    // ---------- JS ---------- //
    // "handler" 'jquery'
    wp_enqueue_script('jquery');
    // Bootstrap
    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.4', true);
    // Eigenes script
    // script, singular
    // true, print in footer, false (default) print in header
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/awesome.js', array(), '1.0.0', true);
    // WP jquery aktivieren
    // true um jquery im footer ausgeben zu lassen


}
// calling function, action
// first hook wp_enqueue_scripts as STRING!
// scripts, plural
add_action('wp_enqueue_scripts', 'awesome_script_enqueue');

// ====================================================== //
// =================== Activate Menus =================== //
// ====================================================== //
function awesome_theme_setup()
{
    add_theme_support('menus');

    // Menü aktivieren/registering
    // menu, singular
    // 2. String, eigene Beschreibung
    // Header Menu
    register_nav_menu('primary', 'Primary Header Navigation');
    // Footer Menu
    register_nav_menu('secondary', 'Footer Navigation');
}
// calling function menus
add_action('init', 'awesome_theme_setup');

// ====================================================== //
// =============== Theme support function =============== //
// ====================================================== //
// hook
// Funktioniert ohne add_action innerhalb der functions.php
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');

add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

// ====================================================== //
// ================== Sidebar function ================== //
// ====================================================== //
function awesome_widget_setup(){
    // registering sidebar
    register_sidebar(
        array(
            'name' => 'Sidebar',
            'id' => 'sidebar-1',
            // wrapper style-class for sidebar
            'class' => 'custom',
            'description' => 'Standard Sidebar',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>'
        )
    );

    // registering sidebar 2
    register_sidebar(
        array(
            'name' => 'Sidebar 2',
            'id' => 'sidebar-2',
            // wrapper style-class for sidebar
            'class' => 'custom',
            'description' => '2nd Standard Sidebar',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>'
        )
    );

    // registering sidebar 3
    register_sidebar(
        array(
            'name' => 'Sidebar 3',
            'id' => 'sidebar-3',
            // wrapper style-class for sidebar
            'class' => 'custom',
            'description' => '3rd Standard Sidebar',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>'
        )
    );
}
// calling func.
// para1 = when called, para2 = function
add_action('widgets_init','awesome_widget_setup');
