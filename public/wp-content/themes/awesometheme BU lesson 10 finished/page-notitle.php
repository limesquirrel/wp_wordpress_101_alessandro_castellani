<?php
/*
    Template Name: Page No Title
*/
get_header(); ?>
<div class="row">
    <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
    <div class="col-xs-12 col-sm-8">
        <?php
        if (have_posts()) {
            while (have_posts()) : the_post(); ?>
                <small>Posted on: <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?>, in category <?php the_category(); ?></small>
                <p><?php the_content(); ?></p>
                <hr />
        <?php endwhile;
        }
        ?>
    </div>
    <div class="col-xs-12 col-sm-4">
        <?php get_sidebar( 'sb2' ); ?>
    </div>
</div> <!-- / .row -->
<?php get_footer(); ?>
<?php //sprintf( the_ID()); 
?>