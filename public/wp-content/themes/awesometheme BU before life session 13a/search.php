<?php get_header(); ?>

<div class="row">

    <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
    <div class="col-xs-12 col-sm-8">

        <div class="row">

            <!-- h1>This the index content</h1 -->
            <?php

            // The WordPress Post Loop
            if (have_posts()) : 

                // (The WordPress Post Loop)
                while (have_posts()) : the_post(); ?>

                    <?php get_template_part('content','search') ?>

                <?php endwhile;

            endif;
            ?>
            <!-- / WP Post Loop -->

        </div><!-- / .row .text-center -->

    </div>
    <div class="col-xs-12 col-sm-4">

        <?php get_sidebar(); ?>

    </div>
</div> <!-- / .row -->

<?php get_footer(); ?>