<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Awesome Theme</title>
    <?php wp_head(); ?>
</head>

<!-- Abfrage ob man in der Home-page ist 
Zum Verständnis: Die Home-Page ist die Page, in der der wp-loop abläuft. Nicht in der Home-page, sondern in der Blog-Page
-->
<?php
if (is_front_page()) :
    // Deklaration php var $awesome_classes
    $awesome_classes = array('awesome-class', 'my-class');
else :
    $awesome_classes = array('no-awesome-class');
endif;
?>

<!-- Initialisierung php var $awesome_classes -->

<body <?php body_class($awesome_classes); ?>>

    <div class="container">

        <div class="row">

            <div class="col-xs-12">

                <!-- Bootstrap navbar -->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Awesome Theme</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'primary',
                                'container' => false,
                                'menu_class' => 'nav navbar-nav navbar-right'
                            ));
                            ?>
                        </div>
                    </div><!-- /.container-fluid -->
                </nav>

                <!-- Menu header navigation in navbar integriert -->
                
            </div> <!-- / .col-xs-12 -->
            <!-- New SEARCH -->
            <div class="search-form-container">
                <?php get_search_form(); ?>
            </div> <!-- / .search-form-container -->

        </div> <!-- end .row -->

        <!-- header, add_theme_support function 
        echo, "calls" attributes. PHP Syntax.  
        get_custom_header ist ein array, mit attributen wie width / height. Hier werden sie ausgelesen. PHP Syntax.
        -->
        <img src="<?php //header_image(); ?>" height="<?php //echo get_custom_header()->height; ?>" width="<?php //echo get_custom_header()->width; ?>" alt="" />