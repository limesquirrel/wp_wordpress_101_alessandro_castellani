<?php get_header(); ?>

    <div class="row">
        <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
        <div class="col-xs-12 col-sm-8">
            <!-- h1>This the index content</h1 -->
            <?php 
                // The WordPress Post Loop
                if(have_posts() ){
                    while( have_posts() ): the_post();  ?>

                        <?php get_template_part('content', get_post_format()); ?>

                    <?php endwhile;
                }
            ?>
        </div>
        <div class="col-xs-12 col-sm-4">
            <?php get_sidebar(); ?>
        </div>  
    </div> <!-- / .row -->            
<?php get_footer(); ?>
