<?php get_header(); ?>

    <div class="row">

        <div class="col-xs-12">
            <?php  
                // ====================================================== //
                // ==================== LETZTER POST ==================== //
                // ====================================================== //
                // var declaration
                $lastBlog = new WP_Query('type=post&posts_per_page=1');

                // The WordPress Post Loop (andere Syntax)
                if( $lastBlog->have_posts() ):
                    while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
                        <?php get_template_part('content', get_post_format()); ?>
                    <?php endwhile;
                endif; // / if WP LOOP

                wp_reset_postdata();
            ?>
        </div>


        <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
        <div class="col-xs-12 col-sm-8">
            <!-- h1>This the index content</h1 -->
            <?php 
                // ====================================================== //
                // ============== ALTER INDEX WP POST LOOP ============== //
                // ====================================================== //
                // The WordPress Post Loop
                if( have_posts() ){
                    while( have_posts() ): the_post();  ?>

                        <?php get_template_part('content', get_post_format()); ?>

                    <?php endwhile;
                } // / if WP LOOP

                // ====================================================== //
                // ============ VOR UND VORVOR LETZTEN POSTS ============ //
                // ====================================================== //
                // PRINT OTHER 2 POSTS NOT THE FIRST ONE
                  // var declaration
                  $args = array(
                    'type' => 'post',
                    'posts_per_page' => 2,
                    'offset' => '1'
                  );
                  //$lastBlog = new WP_Query('type=post&posts_per_page=2&offset=1');
                  $lastBlog = new WP_Query($args);

                  // The WordPress Post Loop (andere Syntax)
                  if( $lastBlog->have_posts() ):
                      while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
                          <?php get_template_part('content', get_post_format()); ?>
                      <?php endwhile;
                  endif; // / if WP LOOP
  
                  wp_reset_postdata();
            ?>
            <hr />
            <hr />
            <?php

                // ====================================================== //
                // ================ PRINT ONLY TUTORIALS ================ //
                // ====================================================== //
                  // var declaration
                  // -1 means unlimited posts shown
                  //$lastBlog = new WP_Query('type=post&posts_per_page=-1&cat=13');
                  $lastBlog = new WP_Query('type=post&posts_per_page=-1&category_name=tutorials');

                  // The WordPress Post Loop (andere Syntax)
                  if( $lastBlog->have_posts() ):
                      while( $lastBlog->have_posts() ): $lastBlog->the_post(); ?>
                          <?php get_template_part('content', get_post_format()); ?>
                      <?php endwhile;
                  endif; // / if WP LOOP
  
                  wp_reset_postdata();
            ?>
        </div>
        <div class="col-xs-12 col-sm-4">
            <?php get_sidebar(); ?>
        </div>  
    </div> <!-- / .row -->            
<?php get_footer(); ?>
