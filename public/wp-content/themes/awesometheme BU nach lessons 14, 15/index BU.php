<?php get_header(); ?>

<div class="row">

    <!-- xs, phone, 12, full size, sm, tablet, 8 of 12 columns -->
    <div class="col-xs-12 col-sm-8">

        <div class="row text-center">

            <!-- h1>This the index content</h1 -->
            <?php

            // The WordPress Post Loop
            if (have_posts()) : $i = 0;

                // (The WordPress Post Loop)
                while (have_posts()) : the_post(); ?>

                    <?php //get_template_part('content', get_post_format()); 
                    ?>

                    <!--
                // ====================================================== //
                // ====================== FULL SIZE ===================== //
                // ====================================================== //
                -->
                    <?php if ($i == 0) : ?>

                        <div class="col-xs-12">
                            <!-- Content aus content-featured.php -->
                            <?php
                            if (has_post_thumbnail()) : ?>
                                <div class="thumbnail"><?php the_post_thumbnail('thumbnail'); ?></div>
                            <?php endif; ?>

                            <?php the_title(sprintf('<h1 class="entry-title"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>

                            <small><?php the_category(' '); ?></small>
                        </div> <!-- / .col-xs-12 -->

                        <!--
                // ====================================================== //
                // ====================== HALF SIZE ===================== //
                // ====================================================== //
                -->
                    <?php elseif ($i > 0 && $i <= 2) : ?>
                        <div class="col-xs-6">
                            <!-- Content aus content-featured.php -->
                            <?php
                            if (has_post_thumbnail()) : ?>
                                <div class="thumbnail"><?php the_post_thumbnail('thumbnail'); ?></div>
                            <?php endif; ?>

                            <?php the_title(sprintf('<h1 class="entry-title"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>

                            <small><?php the_category(' '); ?></small>
                        </div> <!-- / .col-xs-6 -->


                        <!--
                // ====================================================== //
                // ====================== THIRD SIZE ==================== //
                // ====================================================== //
                -->
                    <?php elseif ($i > 2) : ?>
                        <div class="col-xs-4">
                            <!-- Content aus content-featured.php -->
                            <?php
                            if (has_post_thumbnail()) : ?>
                                <div class="thumbnail"><?php the_post_thumbnail('thumbnail'); ?></div>
                            <?php endif; ?>

                            <?php the_title(sprintf('<h1 class="entry-title"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>

                            <small><?php the_category(' '); ?></small>
                        </div> <!-- / .col-xs-4 -->

                    <?php endif; ?>

                <?php $i++;
                endwhile;

            endif;
            ?>
            <!-- / WP Post Loop -->

        </div><!-- / .row .text-center -->

    </div>
    <div class="col-xs-12 col-sm-4">

        <?php get_sidebar(); ?>

    </div>
</div> <!-- / .row -->

<?php get_footer(); ?>