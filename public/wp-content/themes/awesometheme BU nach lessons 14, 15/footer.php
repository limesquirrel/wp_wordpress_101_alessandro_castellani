<footer>
    <p>This is the footer area. </p>

    <!-- Menu header navigation -->
    <?php wp_nav_menu(array(
        'theme_location' => 'secondary'
    )); ?>

</footer>

</div> <!-- end .container -->

<?php wp_footer(); ?>

</body>

</html>