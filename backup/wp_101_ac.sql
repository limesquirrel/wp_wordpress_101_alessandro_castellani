# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.5-10.4.6-MariaDB)
# Datenbank: wp_wordpress_101_alessandro_castellani
# Erstellt am: 2020-12-28 14:56:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Export von Tabelle wp_commentmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_commentmeta`;

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle wp_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_comments`;

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`)
VALUES
	(1,1,'Ein WordPress-Kommentator','wapuu@wordpress.example','https://wordpress.org/','','2020-12-13 10:06:37','2020-12-13 09:06:37','Hallo, dies ist ein Kommentar.\nUm mit dem Freischalten, Bearbeiten und Löschen von Kommentaren zu beginnen, besuche bitte die Kommentare-Ansicht im Dashboard.\nDie Avatare der Kommentatoren kommen von <a href=\"https://gravatar.com\">Gravatar</a>.',0,'post-trashed','','comment',0,0);

/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_links`;

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle wp_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_options`;

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`)
VALUES
	(1,'siteurl','http://wordpress-101-alessandro-castellani.lab','yes'),
	(2,'home','http://wordpress-101-alessandro-castellani.lab','yes'),
	(3,'blogname','wordpress-101-alessandro-castellani','yes'),
	(4,'blogdescription','tutorial','yes'),
	(5,'users_can_register','0','yes'),
	(6,'admin_email','reichenbach.s@gmail.com','yes'),
	(7,'start_of_week','1','yes'),
	(8,'use_balanceTags','0','yes'),
	(9,'use_smilies','1','yes'),
	(10,'require_name_email','1','yes'),
	(11,'comments_notify','1','yes'),
	(12,'posts_per_rss','10','yes'),
	(13,'rss_use_excerpt','0','yes'),
	(14,'mailserver_url','mail.example.com','yes'),
	(15,'mailserver_login','login@example.com','yes'),
	(16,'mailserver_pass','password','yes'),
	(17,'mailserver_port','110','yes'),
	(18,'default_category','1','yes'),
	(19,'default_comment_status','','yes'),
	(20,'default_ping_status','open','yes'),
	(21,'default_pingback_flag','','yes'),
	(22,'posts_per_page','10','yes'),
	(23,'date_format','j. F Y','yes'),
	(24,'time_format','G:i','yes'),
	(25,'links_updated_date_format','j. F Y G:i','yes'),
	(26,'comment_moderation','','yes'),
	(27,'moderation_notify','1','yes'),
	(28,'permalink_structure','/%postname%/','yes'),
	(29,'rewrite_rules','a:127:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:12:\"portfolio/?$\";s:29:\"index.php?post_type=portfolio\";s:42:\"portfolio/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=portfolio&feed=$matches[1]\";s:37:\"portfolio/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=portfolio&feed=$matches[1]\";s:29:\"portfolio/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=portfolio&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:37:\"portfolio/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"portfolio/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"portfolio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"portfolio/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"portfolio/([^/]+)/embed/?$\";s:42:\"index.php?portfolio=$matches[1]&embed=true\";s:30:\"portfolio/([^/]+)/trackback/?$\";s:36:\"index.php?portfolio=$matches[1]&tb=1\";s:50:\"portfolio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?portfolio=$matches[1]&feed=$matches[2]\";s:45:\"portfolio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?portfolio=$matches[1]&feed=$matches[2]\";s:38:\"portfolio/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&paged=$matches[2]\";s:45:\"portfolio/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&cpage=$matches[2]\";s:34:\"portfolio/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?portfolio=$matches[1]&page=$matches[2]\";s:26:\"portfolio/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"portfolio/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"portfolio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"portfolio/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:46:\"field/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?field=$matches[1]&feed=$matches[2]\";s:41:\"field/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?field=$matches[1]&feed=$matches[2]\";s:22:\"field/([^/]+)/embed/?$\";s:38:\"index.php?field=$matches[1]&embed=true\";s:34:\"field/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?field=$matches[1]&paged=$matches[2]\";s:16:\"field/([^/]+)/?$\";s:27:\"index.php?field=$matches[1]\";s:49:\"software/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?software=$matches[1]&feed=$matches[2]\";s:44:\"software/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?software=$matches[1]&feed=$matches[2]\";s:25:\"software/([^/]+)/embed/?$\";s:41:\"index.php?software=$matches[1]&embed=true\";s:37:\"software/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?software=$matches[1]&paged=$matches[2]\";s:19:\"software/([^/]+)/?$\";s:30:\"index.php?software=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=8&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes'),
	(30,'hack_file','0','yes'),
	(31,'blog_charset','UTF-8','yes'),
	(32,'moderation_keys','','no'),
	(33,'active_plugins','a:2:{i:0;s:33:\"classic-editor/classic-editor.php\";i:1;s:33:\"duplicate-post/duplicate-post.php\";}','yes'),
	(34,'category_base','','yes'),
	(35,'ping_sites','http://rpc.pingomatic.com/','yes'),
	(36,'comment_max_links','2','yes'),
	(37,'gmt_offset','','yes'),
	(38,'default_email_category','1','yes'),
	(39,'recently_edited','','no'),
	(40,'template','awesometheme','yes'),
	(41,'stylesheet','awesometheme','yes'),
	(42,'comment_registration','','yes'),
	(43,'html_type','text/html','yes'),
	(44,'use_trackback','0','yes'),
	(45,'default_role','subscriber','yes'),
	(46,'db_version','49752','yes'),
	(47,'uploads_use_yearmonth_folders','','yes'),
	(48,'upload_path','','yes'),
	(49,'blog_public','0','yes'),
	(50,'default_link_category','2','yes'),
	(51,'show_on_front','page','yes'),
	(52,'tag_base','','yes'),
	(53,'show_avatars','','yes'),
	(54,'avatar_rating','G','yes'),
	(55,'upload_url_path','','yes'),
	(56,'thumbnail_size_w','150','yes'),
	(57,'thumbnail_size_h','150','yes'),
	(58,'thumbnail_crop','1','yes'),
	(59,'medium_size_w','300','yes'),
	(60,'medium_size_h','300','yes'),
	(61,'avatar_default','mystery','yes'),
	(62,'large_size_w','1024','yes'),
	(63,'large_size_h','1024','yes'),
	(64,'image_default_link_type','','yes'),
	(65,'image_default_size','','yes'),
	(66,'image_default_align','','yes'),
	(67,'close_comments_for_old_posts','','yes'),
	(68,'close_comments_days_old','14','yes'),
	(69,'thread_comments','1','yes'),
	(70,'thread_comments_depth','5','yes'),
	(71,'page_comments','','yes'),
	(72,'comments_per_page','50','yes'),
	(73,'default_comments_page','newest','yes'),
	(74,'comment_order','asc','yes'),
	(75,'sticky_posts','a:0:{}','yes'),
	(76,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(77,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),
	(78,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),
	(79,'uninstall_plugins','a:0:{}','no'),
	(80,'timezone_string','Europe/Berlin','yes'),
	(81,'page_for_posts','28','yes'),
	(82,'page_on_front','8','yes'),
	(83,'default_post_format','0','yes'),
	(84,'link_manager_enabled','0','yes'),
	(85,'finished_splitting_shared_terms','1','yes'),
	(86,'site_icon','0','yes'),
	(87,'medium_large_size_w','768','yes'),
	(88,'medium_large_size_h','0','yes'),
	(89,'wp_page_for_privacy_policy','3','yes'),
	(90,'show_comments_cookies_opt_in','1','yes'),
	(91,'admin_email_lifespan','1623402397','yes'),
	(92,'disallowed_keys','','no'),
	(93,'comment_previously_approved','','yes'),
	(94,'auto_plugin_theme_update_emails','a:0:{}','no'),
	(95,'auto_update_core_dev','enabled','yes'),
	(96,'auto_update_core_minor','enabled','yes'),
	(97,'auto_update_core_major','enabled','yes'),
	(98,'initial_db_version','49752','yes'),
	(99,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),
	(100,'fresh_site','0','yes'),
	(101,'WPLANG','de_DE','yes'),
	(102,'widget_search','a:3:{i:2;a:1:{s:5:\"title\";s:0:\"\";}i:3;a:1:{s:5:\"title\";s:6:\"Search\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(103,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(104,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(105,'widget_archives','a:3:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}i:3;a:3:{s:5:\"title\";s:7:\"Archive\";s:5:\"count\";i:1;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(106,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(107,'sidebars_widgets','a:5:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-3\";i:1;s:10:\"nav_menu-2\";i:2;s:10:\"archives-3\";}s:9:\"sidebar-2\";a:1:{i:0;s:13:\"media_image-2\";}s:9:\"sidebar-3\";a:1:{i:0;s:10:\"calendar-2\";}s:13:\"array_version\";i:3;}','yes'),
	(108,'cron','a:7:{i:1609167998;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1609189598;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1609232797;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1609232821;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1609232826;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1609751197;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}','yes'),
	(109,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(110,'widget_calendar','a:2:{i:2;a:1:{s:5:\"title\";s:26:\"Sidebar 3 widget, Kalender\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(111,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(112,'widget_media_image','a:2:{i:2;a:15:{s:4:\"size\";s:6:\"medium\";s:5:\"width\";i:1920;s:6:\"height\";i:1146;s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:9:\"link_type\";s:6:\"custom\";s:8:\"link_url\";s:0:\"\";s:13:\"image_classes\";s:0:\"\";s:12:\"link_classes\";s:0:\"\";s:8:\"link_rel\";s:0:\"\";s:17:\"link_target_blank\";b:0;s:11:\"image_title\";s:0:\"\";s:13:\"attachment_id\";i:53;s:3:\"url\";s:83:\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art-2-300x179.jpg\";s:5:\"title\";s:22:\"Sidebar-2 widget, Bild\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(113,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(114,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(115,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(116,'widget_nav_menu','a:2:{i:2;a:2:{s:5:\"title\";s:4:\"Menu\";s:8:\"nav_menu\";i:3;}s:12:\"_multiwidget\";i:1;}','yes'),
	(117,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(119,'theme_mods_twentytwentyone','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1607850638;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes'),
	(129,'can_compress_scripts','1','no'),
	(144,'recovery_keys','a:0:{}','yes'),
	(145,'new_admin_email','reichenbach.s@gmail.com','yes'),
	(150,'finished_updating_comment_type','1','yes'),
	(164,'current_theme','Awesome Theme','yes'),
	(165,'theme_mods_twentythirteen','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1607850827;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes'),
	(166,'theme_switched','','yes'),
	(168,'theme_mods_twentyten','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1607850655;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:19:\"primary-widget-area\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:21:\"secondary-widget-area\";a:0:{}s:24:\"first-footer-widget-area\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}s:25:\"second-footer-widget-area\";a:0:{}s:24:\"third-footer-widget-area\";a:0:{}s:25:\"fourth-footer-widget-area\";a:0:{}}}}','yes'),
	(171,'theme_mods_twentythirteen child-theme','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1608281613;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes'),
	(177,'_transient_health-check-site-status-result','{\"good\":13,\"recommended\":5,\"critical\":2}','yes'),
	(195,'theme_mods_twentyfifteen','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1608284316;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),
	(196,'_transient_twentyfifteen_categories','1','yes'),
	(200,'theme_mods_awesometheme','a:7:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:2:{s:7:\"primary\";i:2;s:9:\"secondary\";i:3;}s:16:\"background_image\";s:91:\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/light-blue-background.png\";s:21:\"background_position_x\";s:6:\"center\";s:21:\"background_position_y\";s:6:\"center\";s:12:\"header_image\";s:92:\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg\";s:17:\"header_image_data\";O:8:\"stdClass\":5:{s:13:\"attachment_id\";i:37;s:3:\"url\";s:92:\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg\";s:13:\"thumbnail_url\";s:92:\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg\";s:6:\"height\";i:100;s:5:\"width\";i:960;}}','yes'),
	(213,'recently_activated','a:0:{}','yes'),
	(222,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes'),
	(292,'_site_transient_timeout_php_check_1a7f48c14e3f2fff89109b9b85fa5d1b','1609151835','no'),
	(293,'_site_transient_php_check_1a7f48c14e3f2fff89109b9b85fa5d1b','a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no'),
	(384,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/de_DE/wordpress-5.6.zip\";s:6:\"locale\";s:5:\"de_DE\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/de_DE/wordpress-5.6.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"5.6\";s:7:\"version\";s:3:\"5.6\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1609146434;s:15:\"version_checked\";s:3:\"5.6\";s:12:\"translations\";a:0:{}}','no'),
	(385,'_site_transient_update_plugins','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1609146434;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}}}}','no'),
	(386,'_site_transient_update_themes','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1609146442;s:7:\"checked\";a:15:{s:46:\"Backup awesometheme lessons 1 - 7/awesometheme\";s:9:\"0.1 alpha\";s:58:\"awesometheme 7a fertig, nicht benutzt, q a durchgearbeitet\";s:9:\"0.1 alpha\";s:25:\"awesometheme BU after 13a\";s:9:\"0.1 alpha\";s:39:\"awesometheme BU before life session 13a\";s:9:\"0.1 alpha\";s:34:\"awesometheme BU lesson 10 finished\";s:9:\"0.1 alpha\";s:61:\"awesometheme BU lesson 10a erster Teil bis minute 39 finished\";s:9:\"0.1 alpha\";s:30:\"awesometheme BU lesson 17 done\";s:9:\"0.1 alpha\";s:35:\"awesometheme BU nach lessons 14, 15\";s:9:\"0.1 alpha\";s:36:\"awesometheme BU session 10b finished\";s:9:\"0.1 alpha\";s:12:\"awesometheme\";s:9:\"0.1 alpha\";s:9:\"lesson_10\";s:9:\"0.1 alpha\";s:10:\"lesson_10a\";s:9:\"0.1 alpha\";s:13:\"twentyfifteen\";s:3:\"2.8\";s:26:\"twentythirteen child-theme\";s:3:\"3.2\";s:14:\"twentythirteen\";s:3:\"3.2\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:2:{s:13:\"twentyfifteen\";a:6:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"2.8\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.2.8.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";s:5:\"5.2.4\";}s:14:\"twentythirteen\";a:6:{s:5:\"theme\";s:14:\"twentythirteen\";s:11:\"new_version\";s:3:\"3.2\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentythirteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentythirteen.3.2.zip\";s:8:\"requires\";s:3:\"3.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}','no'),
	(387,'duplicate_post_copytitle','1','yes'),
	(388,'duplicate_post_copydate','0','yes'),
	(389,'duplicate_post_copystatus','0','yes'),
	(390,'duplicate_post_copyslug','0','yes'),
	(391,'duplicate_post_copyexcerpt','1','yes'),
	(392,'duplicate_post_copycontent','1','yes'),
	(393,'duplicate_post_copythumbnail','1','yes'),
	(394,'duplicate_post_copytemplate','1','yes'),
	(395,'duplicate_post_copyformat','1','yes'),
	(396,'duplicate_post_copyauthor','0','yes'),
	(397,'duplicate_post_copypassword','0','yes'),
	(398,'duplicate_post_copyattachments','0','yes'),
	(399,'duplicate_post_copychildren','0','yes'),
	(400,'duplicate_post_copycomments','0','yes'),
	(401,'duplicate_post_copymenuorder','1','yes'),
	(402,'duplicate_post_taxonomies_blacklist','a:0:{}','yes'),
	(403,'duplicate_post_blacklist','','yes'),
	(404,'duplicate_post_types_enabled','a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}','yes'),
	(405,'duplicate_post_show_row','1','yes'),
	(406,'duplicate_post_show_adminbar','1','yes'),
	(407,'duplicate_post_show_submitbox','1','yes'),
	(408,'duplicate_post_show_bulkactions','1','yes'),
	(409,'duplicate_post_show_original_column','0','yes'),
	(410,'duplicate_post_show_original_in_post_states','0','yes'),
	(411,'duplicate_post_show_original_meta_box','0','yes'),
	(412,'duplicate_post_show_notice','1','no'),
	(413,'duplicate_post_version','3.2.6','yes'),
	(454,'category_children','a:0:{}','yes'),
	(463,'_site_transient_timeout_theme_roots','1609148227','no'),
	(464,'_site_transient_theme_roots','a:15:{s:46:\"Backup awesometheme lessons 1 - 7/awesometheme\";s:7:\"/themes\";s:58:\"awesometheme 7a fertig, nicht benutzt, q a durchgearbeitet\";s:7:\"/themes\";s:25:\"awesometheme BU after 13a\";s:7:\"/themes\";s:39:\"awesometheme BU before life session 13a\";s:7:\"/themes\";s:34:\"awesometheme BU lesson 10 finished\";s:7:\"/themes\";s:61:\"awesometheme BU lesson 10a erster Teil bis minute 39 finished\";s:7:\"/themes\";s:30:\"awesometheme BU lesson 17 done\";s:7:\"/themes\";s:35:\"awesometheme BU nach lessons 14, 15\";s:7:\"/themes\";s:36:\"awesometheme BU session 10b finished\";s:7:\"/themes\";s:12:\"awesometheme\";s:7:\"/themes\";s:9:\"lesson_10\";s:7:\"/themes\";s:10:\"lesson_10a\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:26:\"twentythirteen child-theme\";s:7:\"/themes\";s:14:\"twentythirteen\";s:7:\"/themes\";}','no'),
	(465,'_site_transient_timeout_php_check_a5907c2ea4d6fbd7e531b3aa7734f0e4','1609751228','no'),
	(466,'_site_transient_php_check_a5907c2ea4d6fbd7e531b3aa7734f0e4','a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:0;}','no'),
	(473,'type_children','a:1:{i:18;a:1:{i:0;i:21;}}','yes'),
	(477,'field_children','a:1:{i:23;a:1:{i:0;i:24;}}','yes');

/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_postmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_postmeta`;

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`)
VALUES
	(1,2,'_wp_page_template','default'),
	(2,3,'_wp_page_template','default'),
	(3,2,'_wp_trash_meta_status','publish'),
	(4,2,'_wp_trash_meta_time','1608363978'),
	(5,2,'_wp_desired_post_slug','beispiel-seite'),
	(6,3,'_edit_last','1'),
	(7,3,'_edit_lock','1608363988:1'),
	(9,8,'_edit_last','1'),
	(10,8,'_edit_lock','1608736138:1'),
	(11,10,'_edit_last','1'),
	(12,10,'_edit_lock','1608985476:1'),
	(13,12,'_edit_last','1'),
	(14,12,'_edit_lock','1608392350:1'),
	(15,14,'_menu_item_type','custom'),
	(16,14,'_menu_item_menu_item_parent','0'),
	(17,14,'_menu_item_object_id','14'),
	(18,14,'_menu_item_object','custom'),
	(19,14,'_menu_item_target',''),
	(20,14,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(21,14,'_menu_item_xfn',''),
	(22,14,'_menu_item_url','http://wordpress-101-alessandro-castellani.lab/'),
	(23,14,'_menu_item_orphaned','1608365019'),
	(24,15,'_menu_item_type','post_type'),
	(25,15,'_menu_item_menu_item_parent','0'),
	(26,15,'_menu_item_object_id','8'),
	(27,15,'_menu_item_object','page'),
	(28,15,'_menu_item_target',''),
	(29,15,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(30,15,'_menu_item_xfn',''),
	(31,15,'_menu_item_url',''),
	(33,16,'_menu_item_type','post_type'),
	(34,16,'_menu_item_menu_item_parent','0'),
	(35,16,'_menu_item_object_id','10'),
	(36,16,'_menu_item_object','page'),
	(37,16,'_menu_item_target',''),
	(38,16,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(39,16,'_menu_item_xfn',''),
	(40,16,'_menu_item_url',''),
	(42,17,'_menu_item_type','post_type'),
	(43,17,'_menu_item_menu_item_parent','16'),
	(44,17,'_menu_item_object_id','12'),
	(45,17,'_menu_item_object','page'),
	(46,17,'_menu_item_target',''),
	(47,17,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(48,17,'_menu_item_xfn',''),
	(49,17,'_menu_item_url',''),
	(51,18,'_menu_item_type','post_type'),
	(52,18,'_menu_item_menu_item_parent','0'),
	(53,18,'_menu_item_object_id','12'),
	(54,18,'_menu_item_object','page'),
	(55,18,'_menu_item_target',''),
	(56,18,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(57,18,'_menu_item_xfn',''),
	(58,18,'_menu_item_url',''),
	(60,19,'_menu_item_type','post_type'),
	(61,19,'_menu_item_menu_item_parent','0'),
	(62,19,'_menu_item_object_id','10'),
	(63,19,'_menu_item_object','page'),
	(64,19,'_menu_item_target',''),
	(65,19,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(66,19,'_menu_item_xfn',''),
	(67,19,'_menu_item_url',''),
	(69,1,'_wp_trash_meta_status','publish'),
	(70,1,'_wp_trash_meta_time','1608385754'),
	(71,1,'_wp_desired_post_slug','hallo-welt'),
	(72,1,'_wp_trash_meta_comments_status','a:1:{i:1;s:1:\"1\";}'),
	(73,22,'_edit_last','1'),
	(74,22,'_edit_lock','1608791463:1'),
	(76,24,'_edit_last','1'),
	(77,24,'_edit_lock','1608643165:1'),
	(79,26,'_edit_last','1'),
	(80,26,'_edit_lock','1608879845:1'),
	(83,28,'_edit_last','1'),
	(84,28,'_edit_lock','1608387167:1'),
	(85,30,'_menu_item_type','post_type'),
	(86,30,'_menu_item_menu_item_parent','0'),
	(87,30,'_menu_item_object_id','28'),
	(88,30,'_menu_item_object','page'),
	(89,30,'_menu_item_target',''),
	(90,30,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(91,30,'_menu_item_xfn',''),
	(92,30,'_menu_item_url',''),
	(93,30,'_menu_item_orphaned','1608387432'),
	(94,31,'_menu_item_type','post_type'),
	(95,31,'_menu_item_menu_item_parent','0'),
	(96,31,'_menu_item_object_id','28'),
	(97,31,'_menu_item_object','page'),
	(98,31,'_menu_item_target',''),
	(99,31,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(100,31,'_menu_item_xfn',''),
	(101,31,'_menu_item_url',''),
	(103,10,'_wp_page_template','page-notitle.php'),
	(104,32,'_wp_attached_file','blue-gradient.jpg'),
	(105,32,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1200;s:4:\"file\";s:17:\"blue-gradient.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"blue-gradient-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"blue-gradient-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"blue-gradient-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"blue-gradient-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:27:\"blue-gradient-1536x1152.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1152;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(106,32,'_wp_attachment_is_custom_background','awesometheme'),
	(107,33,'_wp_trash_meta_status','publish'),
	(108,33,'_wp_trash_meta_time','1608402117'),
	(109,34,'_wp_attached_file','backgroundDefault.jpg'),
	(110,34,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:21:\"backgroundDefault.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"backgroundDefault-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"backgroundDefault-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"backgroundDefault-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(111,34,'_wp_attachment_is_custom_background','awesometheme'),
	(112,35,'_wp_trash_meta_status','publish'),
	(113,35,'_wp_trash_meta_time','1608402276'),
	(116,36,'_wp_trash_meta_status','publish'),
	(117,36,'_wp_trash_meta_time','1608402624'),
	(118,37,'_wp_attached_file','goldeneye-elite-header.jpg'),
	(119,37,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:960;s:6:\"height\";i:100;s:4:\"file\";s:26:\"goldeneye-elite-header.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"goldeneye-elite-header-300x31.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:31;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"goldeneye-elite-header-150x100.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"goldeneye-elite-header-768x80.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:80;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
	(120,38,'_wp_attached_file','light-blue-background.png'),
	(121,38,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:625;s:6:\"height\";i:500;s:4:\"file\";s:25:\"light-blue-background.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"light-blue-background-300x240.png\";s:5:\"width\";i:300;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"light-blue-background-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(122,37,'_wp_attachment_custom_header_last_used_awesometheme','1608403464'),
	(123,37,'_wp_attachment_is_custom_header','awesometheme'),
	(124,38,'_wp_attachment_is_custom_background','awesometheme'),
	(125,39,'_wp_trash_meta_status','publish'),
	(126,39,'_wp_trash_meta_time','1608403464'),
	(127,40,'_wp_attached_file','apple_os.png'),
	(128,40,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:100;s:6:\"height\";i:100;s:4:\"file\";s:12:\"apple_os.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(129,26,'_thumbnail_id','53'),
	(138,46,'_wp_attached_file','malibu-peach-beach.jpg'),
	(139,46,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:600;s:6:\"height\";i:400;s:4:\"file\";s:22:\"malibu-peach-beach.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"malibu-peach-beach-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"malibu-peach-beach-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(140,24,'_thumbnail_id','46'),
	(142,48,'_edit_last','1'),
	(143,48,'_edit_lock','1608740858:1'),
	(144,49,'_wp_attached_file','abstract-2.jpg'),
	(145,49,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1396;s:4:\"file\";s:14:\"abstract-2.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"abstract-2-300x218.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"abstract-2-1024x745.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:745;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"abstract-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"abstract-2-768x558.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:558;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:24:\"abstract-2-1536x1117.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1117;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(146,50,'_wp_attached_file','adwords.jpg'),
	(147,50,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1275;s:4:\"file\";s:11:\"adwords.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"adwords-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"adwords-1024x680.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:680;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"adwords-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"adwords-768x510.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:510;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:21:\"adwords-1536x1020.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1020;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(148,51,'_wp_attached_file','architecture-2.jpg'),
	(149,51,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1280;s:4:\"file\";s:18:\"architecture-2.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"architecture-2-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"architecture-2-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"architecture-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"architecture-2-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:28:\"architecture-2-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(150,52,'_wp_attached_file','architecture-3.jpg'),
	(151,52,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1440;s:4:\"file\";s:18:\"architecture-3.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"architecture-3-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"architecture-3-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"architecture-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"architecture-3-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:28:\"architecture-3-1536x1152.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1152;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(152,53,'_wp_attached_file','art-2.jpg'),
	(153,53,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1146;s:4:\"file\";s:9:\"art-2.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"art-2-300x179.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"art-2-1024x611.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:611;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"art-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"art-2-768x458.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:458;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"art-2-1536x917.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:917;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(154,54,'_wp_attached_file','art.jpg'),
	(155,54,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1279;s:4:\"file\";s:7:\"art.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"art-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"art-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"art-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"art-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:17:\"art-1536x1023.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1023;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(158,56,'_edit_last','1'),
	(159,56,'_edit_lock','1608740864:1'),
	(160,57,'_wp_attached_file','zelda-2-map.png'),
	(161,57,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:3128;s:6:\"height\";i:2128;s:4:\"file\";s:15:\"zelda-2-map.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"zelda-2-map-300x204.png\";s:5:\"width\";i:300;s:6:\"height\";i:204;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"zelda-2-map-1024x697.png\";s:5:\"width\";i:1024;s:6:\"height\";i:697;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"zelda-2-map-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"zelda-2-map-768x522.png\";s:5:\"width\";i:768;s:6:\"height\";i:522;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:25:\"zelda-2-map-1536x1045.png\";s:5:\"width\";i:1536;s:6:\"height\";i:1045;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:25:\"zelda-2-map-2048x1393.png\";s:5:\"width\";i:2048;s:6:\"height\";i:1393;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(163,59,'_edit_last','1'),
	(164,59,'_edit_lock','1608740869:1'),
	(165,60,'_wp_attached_file','Master-Roshi-Quotes.jpg'),
	(166,60,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:23:\"Master-Roshi-Quotes.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"Master-Roshi-Quotes-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Master-Roshi-Quotes-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(168,62,'_edit_last','1'),
	(170,62,'_edit_lock','1608740875:1'),
	(173,62,'_wp_old_slug','62-2'),
	(174,65,'_edit_last','1'),
	(175,65,'_edit_lock','1608740882:1'),
	(177,67,'_edit_last','1'),
	(178,67,'_edit_lock','1608740887:1'),
	(179,68,'_wp_attached_file','track006.mp3'),
	(180,68,'_wp_attachment_metadata','a:20:{s:10:\"dataformat\";s:3:\"mp3\";s:8:\"channels\";i:2;s:11:\"sample_rate\";i:48000;s:7:\"bitrate\";d:308134.1222879684;s:11:\"channelmode\";s:12:\"joint stereo\";s:12:\"bitrate_mode\";s:3:\"vbr\";s:5:\"codec\";s:4:\"LAME\";s:7:\"encoder\";s:15:\"LAME3.97 (beta)\";s:8:\"lossless\";b:0;s:15:\"encoder_options\";s:61:\"--preset fast standard -b255 --lowpass 18600 --resample 48000\";s:17:\"compression_ratio\";d:0.20060815253122943;s:10:\"fileformat\";s:3:\"mp3\";s:8:\"filesize\";i:1876059;s:9:\"mime_type\";s:10:\"audio/mpeg\";s:6:\"length\";i:49;s:16:\"length_formatted\";s:4:\"0:49\";s:4:\"band\";s:15:\"Core Design Ltd\";s:6:\"artist\";s:15:\"Core Design Ltd\";s:5:\"album\";s:17:\"Tomb Raider 2 OST\";s:7:\"comment\";s:28:\"www.tombraiderchronicles.com\";}'),
	(182,70,'_edit_last','1'),
	(183,70,'_edit_lock','1608740892:1'),
	(184,71,'_wp_attached_file','steam-chat.jpg'),
	(185,71,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:263;s:6:\"height\";i:192;s:4:\"file\";s:14:\"steam-chat.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"steam-chat-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(187,56,'_thumbnail_id','57'),
	(189,48,'_thumbnail_id','49'),
	(191,59,'_thumbnail_id','60'),
	(193,70,'_thumbnail_id','71'),
	(196,8,'_wp_page_template','default'),
	(197,74,'_wp_attached_file','wordpress-logo.png'),
	(198,74,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:225;s:6:\"height\";i:225;s:4:\"file\";s:18:\"wordpress-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"wordpress-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(200,79,'_edit_last','1'),
	(201,79,'_edit_lock','1608628117:1'),
	(202,80,'_wp_attached_file','lightbulb.jpg'),
	(203,80,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:310;s:6:\"height\";i:162;s:4:\"file\";s:13:\"lightbulb.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"lightbulb-300x157.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"lightbulb-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(204,81,'_wp_attached_file','rocket.jpg'),
	(205,81,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:275;s:6:\"height\";i:183;s:4:\"file\";s:10:\"rocket.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"rocket-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(206,82,'_wp_attached_file','umbrella.jpg'),
	(207,82,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:300;s:6:\"height\";i:168;s:4:\"file\";s:12:\"umbrella.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"umbrella-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(208,79,'_thumbnail_id','81'),
	(210,84,'_edit_last','1'),
	(211,84,'_edit_lock','1608887091:1'),
	(212,84,'_thumbnail_id','82'),
	(214,86,'_edit_last','1'),
	(215,86,'_edit_lock','1608880446:1'),
	(216,86,'_thumbnail_id','80'),
	(223,88,'_edit_last','1'),
	(224,88,'_edit_lock','1608736291:1'),
	(225,88,'_thumbnail_id','32'),
	(229,91,'_edit_last','1'),
	(230,91,'_edit_lock','1608652423:1'),
	(232,91,'_thumbnail_id','34'),
	(234,91,'_wp_trash_meta_status','publish'),
	(235,91,'_wp_trash_meta_time','1608652585'),
	(236,91,'_wp_desired_post_slug','another-tutorials-post'),
	(237,22,'_thumbnail_id','54'),
	(245,94,'_edit_last','1'),
	(246,94,'_edit_lock','1608887155:1'),
	(247,94,'_thumbnail_id','54'),
	(249,96,'_edit_last','1'),
	(250,96,'_edit_lock','1608889398:1'),
	(251,97,'_wp_attached_file','sc.jpg'),
	(252,97,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:299;s:6:\"height\";i:168;s:4:\"file\";s:6:\"sc.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"sc-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(253,96,'_thumbnail_id','97'),
	(255,99,'_edit_last','1'),
	(256,99,'_edit_lock','1609053621:1'),
	(257,100,'_wp_attached_file','mk23.png'),
	(258,100,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:320;s:6:\"height\";i:224;s:4:\"file\";s:8:\"mk23.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"mk23-300x210.png\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"mk23-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(259,99,'_thumbnail_id','100'),
	(263,102,'_thumbnail_id','80'),
	(264,102,'_dp_original','86'),
	(265,102,'_edit_last','1'),
	(267,102,'_edit_lock','1608889923:1'),
	(268,104,'_thumbnail_id','80'),
	(270,104,'_dp_original','102'),
	(271,104,'_edit_last','1'),
	(273,104,'_edit_lock','1608889916:1'),
	(276,107,'_menu_item_type','custom'),
	(277,107,'_menu_item_menu_item_parent','0'),
	(278,107,'_menu_item_object_id','107'),
	(279,107,'_menu_item_object','custom'),
	(280,107,'_menu_item_target',''),
	(281,107,'_menu_item_classes','a:1:{i:0;s:11:\"open-search\";}'),
	(282,107,'_menu_item_xfn',''),
	(283,107,'_menu_item_url','#'),
	(297,112,'_edit_last','1'),
	(298,112,'_edit_lock','1609160996:1'),
	(299,112,'_wp_old_slug','logo-for-business-a'),
	(300,115,'_edit_last','1'),
	(301,115,'_edit_lock','1609161116:1'),
	(302,117,'_edit_last','1'),
	(303,117,'_edit_lock','1609162038:1'),
	(304,117,'_thumbnail_id','97'),
	(305,115,'_thumbnail_id','57'),
	(306,112,'_thumbnail_id','60'),
	(319,121,'_edit_last','1'),
	(320,121,'_edit_lock','1609151631:1'),
	(321,121,'_wp_page_template','page-portfolio-template.php'),
	(322,123,'_menu_item_type','post_type'),
	(323,123,'_menu_item_menu_item_parent','0'),
	(324,123,'_menu_item_object_id','121'),
	(325,123,'_menu_item_object','page'),
	(326,123,'_menu_item_target',''),
	(327,123,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(328,123,'_menu_item_xfn',''),
	(329,123,'_menu_item_url','');

/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_posts`;

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`)
VALUES
	(1,1,'2020-12-13 10:06:37','2020-12-13 09:06:37','<!-- wp:paragraph -->\n<p>Willkommen bei WordPress. Dies ist dein erster Beitrag. Bearbeite oder lösche ihn und beginne mit dem Schreiben!</p>\n<!-- /wp:paragraph -->','Hallo Welt!','','trash','open','open','','hallo-welt__trashed','','','2020-12-19 14:49:14','2020-12-19 13:49:14','',0,'http://wordpress-101-alessandro-castellani.lab/?p=1',0,'post','',1),
	(2,1,'2020-12-13 10:06:37','2020-12-13 09:06:37','<!-- wp:paragraph -->\n<p>Dies ist eine Beispiel-Seite. Sie unterscheidet sich von Beiträgen, da sie stets an derselben Stelle bleibt und (bei den meisten Themes) in der Website-Navigation angezeigt wird. Die meisten starten mit einem Impressum, der Datenschutzerklärung oder einer „Über&nbsp;uns“-Seite, um sich potenziellen Besuchern der Website vorzustellen. Dort könnte zum Beispiel stehen:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hallo! Tagsüber arbeite ich als Fahrradkurier, nachts bin ich ein aufstrebender Schauspieler und dies hier ist meine Website. Ich lebe in Berlin, habe einen großen Hund namens Jack, mag Pi&#241;a Coladas, jedoch weniger (ohne Schirm) im Regen stehen gelassen zu werden.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...oder so etwas wie das hier:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Das Unternehmen XYZ wurde 1971 gegründet und versorgt die Öffentlichkeit seither mit qualitativ hochwertigen Produkten. An seinem Standort in einer kleinen Großstadt beschäftigt der Betrieb über 2.000 Menschen und unterstützt die Stadtbewohner in vielfacher Hinsicht.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Als neuer WordPress-Benutzer solltest du <a href=\"http://wordpress-101-alessandro-castellani.lab/wp-admin/\">dein Dashboard</a> aufrufen, um diese Seite zu löschen und neue Seiten und Beiträge für deine Website erstellen. Viel Spaß!</p>\n<!-- /wp:paragraph -->','Beispiel-Seite','','trash','closed','open','','beispiel-seite__trashed','','','2020-12-19 08:46:18','2020-12-19 07:46:18','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=2',0,'page','',0),
	(3,1,'2020-12-13 10:06:37','2020-12-13 09:06:37','<!-- wp:heading --><h2>Wer wir sind</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Die Adresse unserer Website ist: http://wordpress-101-alessandro-castellani.lab.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Welche personenbezogenen Daten wir sammeln und warum wir sie sammeln</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Kommentare</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn Besucher Kommentare auf der Website schreiben, sammeln wir die Daten, die im Kommentar-Formular angezeigt werden, außerdem die IP-Adresse des Besuchers und den User-Agent-String (damit wird der Browser identifiziert), um die Erkennung von Spam zu unterstützen.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Aus deiner E-Mail-Adresse kann eine anonymisierte Zeichenfolge erstellt (auch Hash genannt) und dem Gravatar-Dienst übergeben werden, um zu prüfen, ob du diesen benutzt. Die Datenschutzerklärung des Gravatar-Dienstes findest du hier: https://automattic.com/privacy/. Nachdem dein Kommentar freigegeben wurde, ist dein Profilbild öffentlich im Kontext deines Kommentars sichtbar.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Medien</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du ein registrierter Benutzer bist und Fotos auf diese Website lädst, solltest du vermeiden, Fotos mit einem EXIF-GPS-Standort hochzuladen. Besucher dieser Website könnten Fotos, die auf dieser Website gespeichert sind, herunterladen und deren Standort-Informationen extrahieren.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Kontaktformulare</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du einen Kommentar auf unserer Website schreibst, kann das eine Einwilligung sein, deinen Namen, E-Mail-Adresse und Website in Cookies zu speichern. Dies ist eine Komfortfunktion, damit du nicht, wenn du einen weiteren Kommentar schreibst, all diese Daten erneut eingeben musst. Diese Cookies werden ein Jahr lang gespeichert.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Falls du ein Konto hast und dich auf dieser Website anmeldest, werden wir ein temporäres Cookie setzen, um festzustellen, ob dein Browser Cookies akzeptiert. Dieses Cookie enthält keine personenbezogenen Daten und wird verworfen, wenn du deinen Browser schließt.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Wenn du dich anmeldest, werden wir einige Cookies einrichten, um deine Anmeldeinformationen und Anzeigeoptionen zu speichern. Anmelde-Cookies verfallen nach zwei Tagen und Cookies für die Anzeigeoptionen nach einem Jahr. Falls du bei der Anmeldung „Angemeldet bleiben“ auswählst, wird deine Anmeldung zwei Wochen lang aufrechterhalten. Mit der Abmeldung aus deinem Konto werden die Anmelde-Cookies gelöscht.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Wenn du einen Artikel bearbeitest oder veröffentlichst, wird ein zusätzlicher Cookie in deinem Browser gespeichert. Dieser Cookie enthält keine personenbezogenen Daten und verweist nur auf die Beitrags-ID des Artikels, den du gerade bearbeitet hast. Der Cookie verfällt nach einem Tag.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Eingebettete Inhalte von anderen Websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Beiträge auf dieser Website können eingebettete Inhalte beinhalten (z.&nbsp;B. Videos, Bilder, Beiträge etc.). Eingebettete Inhalte von anderen Websites verhalten sich exakt so, als ob der Besucher die andere Website besucht hätte.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Diese Websites können Daten über dich sammeln, Cookies benutzen, zusätzliche Tracking-Dienste von Dritten einbetten und deine Interaktion mit diesem eingebetteten Inhalt aufzeichnen, inklusive deiner Interaktion mit dem eingebetteten Inhalt, falls du ein Konto hast und auf dieser Website angemeldet bist.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analysedienste</h3><!-- /wp:heading --><!-- wp:heading --><h2>Mit wem wir deine Daten teilen</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du eine Zurücksetzung des Passworts beantragst, wird deine IP-Adresse in der E-Mail zur Zurücksetzung enthalten sein.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Wie lange wir deine Daten speichern</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du einen Kommentar schreibst, wird dieser inklusive Metadaten zeitlich unbegrenzt gespeichert. Auf diese Art können wir Folgekommentare automatisch erkennen und freigeben, anstatt sie in einer Moderations-Warteschlange festzuhalten.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Für Benutzer, die sich auf unserer Website registrieren, speichern wir zusätzlich die persönlichen Informationen, die sie in ihren Benutzerprofilen angeben. Alle Benutzer können jederzeit ihre persönlichen Informationen einsehen, verändern oder löschen (der Benutzername kann nicht verändert werden). Administratoren der Website können diese Informationen ebenfalls einsehen und verändern.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Welche Rechte du an deinen Daten hast</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du ein Konto auf dieser Website besitzt oder Kommentare geschrieben hast, kannst du einen Export deiner personenbezogenen Daten bei uns anfordern, inklusive aller Daten, die du uns mitgeteilt hast. Darüber hinaus kannst du die Löschung aller personenbezogenen Daten, die wir von dir gespeichert haben, anfordern. Dies umfasst nicht die Daten, die wir aufgrund administrativer, rechtlicher oder sicherheitsrelevanter Notwendigkeiten aufbewahren müssen.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Wohin wir deine Daten senden</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Besucher-Kommentare könnten von einem automatisierten Dienst zur Spam-Erkennung untersucht werden.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Deine Kontakt-Informationen</h2><!-- /wp:heading --><!-- wp:heading --><h2>Weitere Informationen</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Wie wir deine Daten schützen</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Welche Maßnahmen wir bei Datenschutzverletzungen anbieten</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Von welchen Drittanbietern wir Daten erhalten</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Welche automatisierte Entscheidungsfindung und/oder Profilerstellung wir mit Benutzerdaten durchführen</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Vorausgesetzte Offenlegungspflichten der Industrie</h3><!-- /wp:heading -->','Datenschutzerklärung','','draft','closed','closed','','datenschutzerklaerung','','','2020-12-19 08:46:28','2020-12-19 07:46:28','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=3',200,'page','',0),
	(5,1,'2020-12-19 08:46:18','2020-12-19 07:46:18','<!-- wp:paragraph -->\n<p>Dies ist eine Beispiel-Seite. Sie unterscheidet sich von Beiträgen, da sie stets an derselben Stelle bleibt und (bei den meisten Themes) in der Website-Navigation angezeigt wird. Die meisten starten mit einem Impressum, der Datenschutzerklärung oder einer „Über&nbsp;uns“-Seite, um sich potenziellen Besuchern der Website vorzustellen. Dort könnte zum Beispiel stehen:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hallo! Tagsüber arbeite ich als Fahrradkurier, nachts bin ich ein aufstrebender Schauspieler und dies hier ist meine Website. Ich lebe in Berlin, habe einen großen Hund namens Jack, mag Pi&#241;a Coladas, jedoch weniger (ohne Schirm) im Regen stehen gelassen zu werden.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...oder so etwas wie das hier:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Das Unternehmen XYZ wurde 1971 gegründet und versorgt die Öffentlichkeit seither mit qualitativ hochwertigen Produkten. An seinem Standort in einer kleinen Großstadt beschäftigt der Betrieb über 2.000 Menschen und unterstützt die Stadtbewohner in vielfacher Hinsicht.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Als neuer WordPress-Benutzer solltest du <a href=\"http://wordpress-101-alessandro-castellani.lab/wp-admin/\">dein Dashboard</a> aufrufen, um diese Seite zu löschen und neue Seiten und Beiträge für deine Website erstellen. Viel Spaß!</p>\n<!-- /wp:paragraph -->','Beispiel-Seite','','inherit','closed','closed','','2-revision-v1','','','2020-12-19 08:46:18','2020-12-19 07:46:18','',2,'http://wordpress-101-alessandro-castellani.lab/2-revision-v1/',0,'revision','',0),
	(6,1,'2020-12-19 08:46:28','2020-12-19 07:46:28','<!-- wp:heading --><h2>Wer wir sind</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Die Adresse unserer Website ist: http://wordpress-101-alessandro-castellani.lab.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Welche personenbezogenen Daten wir sammeln und warum wir sie sammeln</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Kommentare</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn Besucher Kommentare auf der Website schreiben, sammeln wir die Daten, die im Kommentar-Formular angezeigt werden, außerdem die IP-Adresse des Besuchers und den User-Agent-String (damit wird der Browser identifiziert), um die Erkennung von Spam zu unterstützen.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Aus deiner E-Mail-Adresse kann eine anonymisierte Zeichenfolge erstellt (auch Hash genannt) und dem Gravatar-Dienst übergeben werden, um zu prüfen, ob du diesen benutzt. Die Datenschutzerklärung des Gravatar-Dienstes findest du hier: https://automattic.com/privacy/. Nachdem dein Kommentar freigegeben wurde, ist dein Profilbild öffentlich im Kontext deines Kommentars sichtbar.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Medien</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du ein registrierter Benutzer bist und Fotos auf diese Website lädst, solltest du vermeiden, Fotos mit einem EXIF-GPS-Standort hochzuladen. Besucher dieser Website könnten Fotos, die auf dieser Website gespeichert sind, herunterladen und deren Standort-Informationen extrahieren.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Kontaktformulare</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du einen Kommentar auf unserer Website schreibst, kann das eine Einwilligung sein, deinen Namen, E-Mail-Adresse und Website in Cookies zu speichern. Dies ist eine Komfortfunktion, damit du nicht, wenn du einen weiteren Kommentar schreibst, all diese Daten erneut eingeben musst. Diese Cookies werden ein Jahr lang gespeichert.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Falls du ein Konto hast und dich auf dieser Website anmeldest, werden wir ein temporäres Cookie setzen, um festzustellen, ob dein Browser Cookies akzeptiert. Dieses Cookie enthält keine personenbezogenen Daten und wird verworfen, wenn du deinen Browser schließt.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Wenn du dich anmeldest, werden wir einige Cookies einrichten, um deine Anmeldeinformationen und Anzeigeoptionen zu speichern. Anmelde-Cookies verfallen nach zwei Tagen und Cookies für die Anzeigeoptionen nach einem Jahr. Falls du bei der Anmeldung „Angemeldet bleiben“ auswählst, wird deine Anmeldung zwei Wochen lang aufrechterhalten. Mit der Abmeldung aus deinem Konto werden die Anmelde-Cookies gelöscht.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Wenn du einen Artikel bearbeitest oder veröffentlichst, wird ein zusätzlicher Cookie in deinem Browser gespeichert. Dieser Cookie enthält keine personenbezogenen Daten und verweist nur auf die Beitrags-ID des Artikels, den du gerade bearbeitet hast. Der Cookie verfällt nach einem Tag.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Eingebettete Inhalte von anderen Websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Beiträge auf dieser Website können eingebettete Inhalte beinhalten (z.&nbsp;B. Videos, Bilder, Beiträge etc.). Eingebettete Inhalte von anderen Websites verhalten sich exakt so, als ob der Besucher die andere Website besucht hätte.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Diese Websites können Daten über dich sammeln, Cookies benutzen, zusätzliche Tracking-Dienste von Dritten einbetten und deine Interaktion mit diesem eingebetteten Inhalt aufzeichnen, inklusive deiner Interaktion mit dem eingebetteten Inhalt, falls du ein Konto hast und auf dieser Website angemeldet bist.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analysedienste</h3><!-- /wp:heading --><!-- wp:heading --><h2>Mit wem wir deine Daten teilen</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du eine Zurücksetzung des Passworts beantragst, wird deine IP-Adresse in der E-Mail zur Zurücksetzung enthalten sein.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Wie lange wir deine Daten speichern</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du einen Kommentar schreibst, wird dieser inklusive Metadaten zeitlich unbegrenzt gespeichert. Auf diese Art können wir Folgekommentare automatisch erkennen und freigeben, anstatt sie in einer Moderations-Warteschlange festzuhalten.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Für Benutzer, die sich auf unserer Website registrieren, speichern wir zusätzlich die persönlichen Informationen, die sie in ihren Benutzerprofilen angeben. Alle Benutzer können jederzeit ihre persönlichen Informationen einsehen, verändern oder löschen (der Benutzername kann nicht verändert werden). Administratoren der Website können diese Informationen ebenfalls einsehen und verändern.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Welche Rechte du an deinen Daten hast</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Wenn du ein Konto auf dieser Website besitzt oder Kommentare geschrieben hast, kannst du einen Export deiner personenbezogenen Daten bei uns anfordern, inklusive aller Daten, die du uns mitgeteilt hast. Darüber hinaus kannst du die Löschung aller personenbezogenen Daten, die wir von dir gespeichert haben, anfordern. Dies umfasst nicht die Daten, die wir aufgrund administrativer, rechtlicher oder sicherheitsrelevanter Notwendigkeiten aufbewahren müssen.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Wohin wir deine Daten senden</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Besucher-Kommentare könnten von einem automatisierten Dienst zur Spam-Erkennung untersucht werden.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Deine Kontakt-Informationen</h2><!-- /wp:heading --><!-- wp:heading --><h2>Weitere Informationen</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Wie wir deine Daten schützen</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Welche Maßnahmen wir bei Datenschutzverletzungen anbieten</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Von welchen Drittanbietern wir Daten erhalten</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Welche automatisierte Entscheidungsfindung und/oder Profilerstellung wir mit Benutzerdaten durchführen</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Vorausgesetzte Offenlegungspflichten der Industrie</h3><!-- /wp:heading -->','Datenschutzerklärung','','inherit','closed','closed','','3-revision-v1','','','2020-12-19 08:46:28','2020-12-19 07:46:28','',3,'http://wordpress-101-alessandro-castellani.lab/3-revision-v1/',0,'revision','',0),
	(8,1,'2020-12-19 08:49:26','2020-12-19 07:49:26','','Home','','publish','closed','closed','','home','','','2020-12-23 14:18:07','2020-12-23 13:18:07','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=8',10,'page','',0),
	(9,1,'2020-12-19 08:49:26','2020-12-19 07:49:26','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.','Home','','inherit','closed','closed','','8-revision-v1','','','2020-12-19 08:49:26','2020-12-19 07:49:26','',8,'http://wordpress-101-alessandro-castellani.lab/8-revision-v1/',0,'revision','',0),
	(10,1,'2020-12-19 08:49:45','2020-12-19 07:49:45','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.\r\n\r\n&nbsp;\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat sem at mauris suscipit porta. Cras metus velit, elementum sed pellentesque a, pharetra eu eros. Etiam facilisis placerat euismod. Nam faucibus neque arcu, quis accumsan leo tincidunt varius. In vel diam enim. Sed id ultrices ligula. Maecenas at urna arcu. Sed quis nulla sapien. Nam felis mauris, tincidunt at convallis id, tempor molestie libero. Quisque viverra sollicitudin nisl sit amet hendrerit. Etiam sit amet arcu sem. Morbi eu nibh condimentum, interdum est quis, tempor nisi. Vivamus convallis erat in pharetra elementum. Phasellus metus neque, commodo vitae venenatis sed, pellentesque non purus. Pellentesque egestas convallis suscipit. Ut luctus, leo quis porta vulputate, purus purus pellentesque ex, id consequat mi nisl quis eros. Integer ornare libero quis risus fermentum consequat. Mauris pharetra odio sagittis, vulputate magna at, lobortis nulla. Proin efficitur, nisi vel finibus elementum, orci sem volutpat eros, eget ultrices velit mi sagittis massa. Vestibulum sagittis ullamcorper cursus. Ut turpis dolor, tempor ut tellus et, sodales ultricies elit. Ut pharetra tristique est ac dictum. Integer ac consectetur purus, vehicula efficitur urna. Donec ultrices accumsan ipsum vitae faucibus. Quisque malesuada ex nisi, a bibendum erat commodo in. Pellentesque suscipit varius gravida. Nam scelerisque est sit amet laoreet pharetra. Vivamus quis ligula sed lacus mattis mollis. Vivamus facilisis orci rutrum nulla porta dignissim. Fusce fermentum id nibh laoreet volutpat. Suspendisse venenatis, risus sed euismod finibus, risus arcu fringilla augue, nec vulputate felis nisl et enim. In ornare, massa a cursus cursus, nisl mi ornare mauris, nec porttitor risus erat ut odio. Integer malesuada hendrerit purus ullamcorper molestie. Fusce imperdiet orci vitae purus suscipit rutrum. Nunc ac metus ac mi commodo sagittis at in leo. Vestibulum est lorem, consequat vel dictum non, imperdiet eu felis. Vivamus molestie sapien id quam rutrum, eget hendrerit eros finibus. Morbi in justo at felis blandit fringilla at faucibus purus. Donec ac aliquet purus, vitae tincidunt nulla. Curabitur ultricies auctor quam tincidunt molestie. Quisque tristique metus nunc, in pretium lectus dictum at. Nullam scelerisque placerat dui, maximus commodo augue mollis quis. Nulla gravida ex sed lectus consectetur laoreet et vel ex. Proin pretium libero non leo mattis, ullamcorper scelerisque diam hendrerit. Quisque gravida, dui a porttitor interdum, velit quam pulvinar arcu, eu blandit sapien velit ut lacus. Vestibulum congue eleifend odio, in aliquam leo sodales sed. Nam eu lacinia ipsum. Aenean sagittis nisl eu sodales lacinia. Duis nulla ante, suscipit a nibh sit amet, blandit luctus elit. Etiam a pellentesque lectus. Morbi pulvinar dolor a mi volutpat, sit amet finibus est mattis. Nunc egestas feugiat odio sed mollis. Vestibulum accumsan, velit id facilisis fermentum, nisi risus faucibus nulla, non condimentum mauris tortor eu dui. In urna ante, consectetur ac nunc in, hendrerit elementum lacus. Pellentesque pharetra felis sit amet erat pellentesque malesuada. Sed vel placerat lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque sit amet bibendum diam, sit amet convallis mi. Sed tristique leo sit amet urna lacinia ullamcorper sit amet non sem. Aliquam erat volutpat. Etiam accumsan sit amet odio sit amet posuere. Sed lobortis auctor est a mollis. Duis leo leo, condimentum finibus magna et, tempus tincidunt nisi. Praesent quis molestie elit. Mauris ante orci, luctus et lectus nec, vestibulum suscipit sem. Duis consequat felis id est pellentesque, sit amet vulputate dui commodo. Vestibulum efficitur, elit eu mattis faucibus, turpis nisl lacinia sapien, nec mattis ante massa non felis. Nulla at gravida orci, nec blandit nisl. Duis sed erat a tortor accumsan scelerisque. Donec congue quam vel urna consequat, id sagittis ante pretium. Vivamus posuere, arcu nec tincidunt eleifend, ligula metus convallis odio, nec pulvinar dui leo at tortor. Suspendisse potenti. Sed a euismod mi. Maecenas suscipit luctus enim, ut facilisis elit venenatis ut. Aenean ullamcorper at erat a dictum. Donec vel nulla elit. Aliquam pharetra massa elit, sed maximus lectus mollis ut. Phasellus a ligula et neque fringilla efficitur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam euismod lobortis enim et condimentum. Suspendisse pharetra.','About Me','','publish','closed','closed','','about','','','2020-12-26 09:55:21','2020-12-26 08:55:21','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=10',11,'page','',0),
	(12,1,'2020-12-19 08:50:08','2020-12-19 07:50:08','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.','Kontakt','','publish','closed','closed','','kontakt','','','2020-12-19 13:25:45','2020-12-19 12:25:45','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=12',190,'page','',0),
	(13,1,'2020-12-19 08:50:08','2020-12-19 07:50:08','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.','Kontact','','inherit','closed','closed','','12-revision-v1','','','2020-12-19 08:50:08','2020-12-19 07:50:08','',12,'http://wordpress-101-alessandro-castellani.lab/12-revision-v1/',0,'revision','',0),
	(14,1,'2020-12-19 09:03:39','0000-00-00 00:00:00','','Startseite','','draft','closed','closed','','','','','2020-12-19 09:03:39','0000-00-00 00:00:00','',0,'http://wordpress-101-alessandro-castellani.lab/?p=14',1,'nav_menu_item','',0),
	(15,1,'2020-12-19 09:09:01','2020-12-19 08:09:01',' ','','','publish','closed','closed','','15','','','2020-12-28 09:12:48','2020-12-28 08:12:48','',0,'http://wordpress-101-alessandro-castellani.lab/?p=15',1,'nav_menu_item','',0),
	(16,1,'2020-12-19 09:09:01','2020-12-19 08:09:01',' ','','','publish','closed','closed','','16','','','2020-12-28 09:12:48','2020-12-28 08:12:48','',0,'http://wordpress-101-alessandro-castellani.lab/?p=16',4,'nav_menu_item','',0),
	(17,1,'2020-12-19 09:09:01','2020-12-19 08:09:01',' ','','','publish','closed','closed','','17','','','2020-12-28 09:12:48','2020-12-28 08:12:48','',0,'http://wordpress-101-alessandro-castellani.lab/?p=17',5,'nav_menu_item','',0),
	(18,1,'2020-12-19 09:29:17','2020-12-19 08:29:17',' ','','','publish','closed','closed','','18','','','2020-12-19 09:29:17','2020-12-19 08:29:17','',0,'http://wordpress-101-alessandro-castellani.lab/?p=18',2,'nav_menu_item','',0),
	(19,1,'2020-12-19 09:29:17','2020-12-19 08:29:17',' ','','','publish','closed','closed','','19','','','2020-12-19 09:29:17','2020-12-19 08:29:17','',0,'http://wordpress-101-alessandro-castellani.lab/?p=19',1,'nav_menu_item','',0),
	(20,1,'2020-12-19 13:25:34','2020-12-19 12:25:34','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.','Kontakt','','inherit','closed','closed','','12-revision-v1','','','2020-12-19 13:25:34','2020-12-19 12:25:34','',12,'http://wordpress-101-alessandro-castellani.lab/12-revision-v1/',0,'revision','',0),
	(21,1,'2020-12-19 14:49:14','2020-12-19 13:49:14','<!-- wp:paragraph -->\n<p>Willkommen bei WordPress. Dies ist dein erster Beitrag. Bearbeite oder lösche ihn und beginne mit dem Schreiben!</p>\n<!-- /wp:paragraph -->','Hallo Welt!','','inherit','closed','closed','','1-revision-v1','','','2020-12-19 14:49:14','2020-12-19 13:49:14','',1,'http://wordpress-101-alessandro-castellani.lab/1-revision-v1/',0,'revision','',0),
	(22,1,'2020-12-19 14:49:25','2020-12-19 13:49:25','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Post 1 Lorem Ipsum','','publish','closed','open','','post-1','','','2020-12-24 07:33:15','2020-12-24 06:33:15','',0,'http://wordpress-101-alessandro-castellani.lab/?p=22',0,'post','',0),
	(23,1,'2020-12-19 14:49:25','2020-12-19 13:49:25','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Post 1','','inherit','closed','closed','','22-revision-v1','','','2020-12-19 14:49:25','2020-12-19 13:49:25','',22,'http://wordpress-101-alessandro-castellani.lab/22-revision-v1/',0,'revision','',0),
	(24,1,'2020-12-19 14:49:35','2020-12-19 13:49:35','<table class=\"ingredients table-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 Dose/n</td>\r\n<td class=\"td-right\" width=\"66%\">Pfirsich(e)</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">½ Flasche</td>\r\n<td class=\"td-right\" width=\"66%\">Malibu Coconut Rum</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 ½ Liter</td>\r\n<td class=\"td-right\" width=\"66%\">Maracujasaft</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 Flasche</td>\r\n<td class=\"td-right\" width=\"66%\">Sekt, trocken</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2>Zubereitung</h2>\r\n<small class=\"ds-recipe-meta rds-recipe-meta\"><i class=\"material-icons\"></i> Arbeitszeit ca. 15 Minuten <i class=\"material-icons\"></i> Ruhezeit ca. 1 Tag <i class=\"material-icons\"></i> Gesamtzeit ca. 1 Tag 15 Minuten </small>\r\n<div class=\"ds-box\">\r\n\r\nAm Abend davor die Pfirsiche klein schneiden und mit etwas von der Flüssigkeit aus der Dose in den Malibu einlegen. Anschließend 24 Stunden kaltstellen.\r\n\r\nAm nächsten Tag mit dem gekühlten Sekt und Maracujasaft auffüllen und genießen.\r\n\r\n</div>','Recipe Malibu Peach Beach','','publish','closed','open','','post-2','','','2020-12-22 14:19:25','2020-12-22 13:19:25','',0,'http://wordpress-101-alessandro-castellani.lab/?p=24',0,'post','',0),
	(26,1,'2020-12-19 14:49:50','2020-12-19 13:49:50','<div class=\"quoteDetails\">\r\n<div class=\"quoteText\">“There is something to be learned from a rainstorm. When meeting with a sudden shower, you try not to get wet and run quickly along the road. But doing such things as passing under the eaves of houses, you still get wet. When you are resolved from the beginning, you will not be perplexed, though you will still get the same soaking. This understanding extends to everything.”\r\n― <span class=\"authorOrTitle\"> Tsunetomo Yamamoto, </span> <span id=\"quote_book_link_283747\"> <a class=\"authorOrTitle\" href=\"https://www.goodreads.com/work/quotes/275268\">The Hagakure: A code to the way of samurai</a></span></div>\r\n<div></div>\r\n<div><a href=\"https://www.goodreads.com/work/quotes/275268\" target=\"_blank\" rel=\"noopener\">Source</a></div>\r\n</div>','Poem','','publish','closed','open','','post-3','','','2020-12-24 07:33:45','2020-12-24 06:33:45','',0,'http://wordpress-101-alessandro-castellani.lab/?p=26',0,'post','',0),
	(27,1,'2020-12-19 14:49:50','2020-12-19 13:49:50','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Post 3','','inherit','closed','closed','','26-revision-v1','','','2020-12-19 14:49:50','2020-12-19 13:49:50','',26,'http://wordpress-101-alessandro-castellani.lab/26-revision-v1/',0,'revision','',0),
	(28,1,'2020-12-19 15:15:07','2020-12-19 14:15:07','','Blog','','publish','closed','closed','','blog','','','2020-12-19 15:15:07','2020-12-19 14:15:07','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=28',30,'page','',0),
	(29,1,'2020-12-19 15:15:07','2020-12-19 14:15:07','','Blog','','inherit','closed','closed','','28-revision-v1','','','2020-12-19 15:15:07','2020-12-19 14:15:07','',28,'http://wordpress-101-alessandro-castellani.lab/28-revision-v1/',0,'revision','',0),
	(30,1,'2020-12-19 15:17:12','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2020-12-19 15:17:12','0000-00-00 00:00:00','',0,'http://wordpress-101-alessandro-castellani.lab/?p=30',1,'nav_menu_item','',0),
	(31,1,'2020-12-19 15:17:31','2020-12-19 14:17:31',' ','','','publish','closed','closed','','31','','','2020-12-28 09:12:48','2020-12-28 08:12:48','',0,'http://wordpress-101-alessandro-castellani.lab/?p=31',2,'nav_menu_item','',0),
	(32,1,'2020-12-19 19:20:56','2020-12-19 18:20:56','','blue-gradient','','inherit','','closed','','blue-gradient','','','2020-12-19 19:20:56','2020-12-19 18:20:56','',0,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/blue-gradient.jpg',0,'attachment','image/jpeg',0),
	(33,1,'2020-12-19 19:21:57','2020-12-19 18:21:57','{\n    \"awesometheme::background_image\": {\n        \"value\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/blue-gradient.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:21:57\"\n    },\n    \"awesometheme::background_position_x\": {\n        \"value\": \"center\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:21:57\"\n    },\n    \"awesometheme::background_position_y\": {\n        \"value\": \"center\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:21:57\"\n    }\n}','','','trash','closed','closed','','aec4fec1-7950-45de-819b-131e65a4ac0a','','','2020-12-19 19:21:57','2020-12-19 18:21:57','',0,'http://wordpress-101-alessandro-castellani.lab/aec4fec1-7950-45de-819b-131e65a4ac0a/',0,'customize_changeset','',0),
	(34,1,'2020-12-19 19:24:29','2020-12-19 18:24:29','','backgroundDefault','','inherit','','closed','','backgrounddefault','','','2020-12-19 19:24:29','2020-12-19 18:24:29','',0,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/backgroundDefault.jpg',0,'attachment','image/jpeg',0),
	(35,1,'2020-12-19 19:24:36','2020-12-19 18:24:36','{\n    \"awesometheme::background_image\": {\n        \"value\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/backgroundDefault.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:24:36\"\n    }\n}','','','trash','closed','closed','','f4be70ea-b3b7-46a7-b794-70520129f1fb','','','2020-12-19 19:24:36','2020-12-19 18:24:36','',0,'http://wordpress-101-alessandro-castellani.lab/f4be70ea-b3b7-46a7-b794-70520129f1fb/',0,'customize_changeset','',0),
	(36,1,'2020-12-19 19:30:24','2020-12-19 18:30:24','{\n    \"awesometheme::header_image\": {\n        \"value\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/blue-gradient.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:30:24\"\n    },\n    \"awesometheme::header_image_data\": {\n        \"value\": {\n            \"url\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/blue-gradient.jpg\",\n            \"thumbnail_url\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/blue-gradient.jpg\",\n            \"timestamp\": 1608402615216,\n            \"attachment_id\": 32,\n            \"width\": 1600,\n            \"height\": 1200\n        },\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:30:24\"\n    }\n}','','','trash','closed','closed','','565bcc9f-e0b8-4826-806e-12fd94fb4050','','','2020-12-19 19:30:24','2020-12-19 18:30:24','',0,'http://wordpress-101-alessandro-castellani.lab/565bcc9f-e0b8-4826-806e-12fd94fb4050/',0,'customize_changeset','',0),
	(37,1,'2020-12-19 19:43:32','2020-12-19 18:43:32','','goldeneye-elite-header','','inherit','','closed','','goldeneye-elite-header','','','2020-12-19 19:43:32','2020-12-19 18:43:32','',0,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg',0,'attachment','image/jpeg',0),
	(38,1,'2020-12-19 19:43:55','2020-12-19 18:43:55','','light-blue-background','','inherit','','closed','','light-blue-background','','','2020-12-19 19:43:55','2020-12-19 18:43:55','',0,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/light-blue-background.png',0,'attachment','image/png',0),
	(39,1,'2020-12-19 19:44:24','2020-12-19 18:44:24','{\n    \"awesometheme::header_image\": {\n        \"value\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:44:24\"\n    },\n    \"awesometheme::header_image_data\": {\n        \"value\": {\n            \"url\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg\",\n            \"thumbnail_url\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/goldeneye-elite-header.jpg\",\n            \"timestamp\": 1608403453050,\n            \"attachment_id\": 37,\n            \"width\": 960,\n            \"height\": 100\n        },\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:44:24\"\n    },\n    \"awesometheme::background_image\": {\n        \"value\": \"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/light-blue-background.png\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-19 18:44:24\"\n    }\n}','','','trash','closed','closed','','6fd728f9-4c27-4de2-8cd0-fc39fa5c1ae7','','','2020-12-19 19:44:24','2020-12-19 18:44:24','',0,'http://wordpress-101-alessandro-castellani.lab/6fd728f9-4c27-4de2-8cd0-fc39fa5c1ae7/',0,'customize_changeset','',0),
	(40,1,'2020-12-19 19:55:52','2020-12-19 18:55:52','','apple_os','','inherit','','closed','','apple_os','','','2020-12-19 19:55:52','2020-12-19 18:55:52','',26,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/apple_os.png',0,'attachment','image/png',0),
	(41,1,'2020-12-20 11:02:12','2020-12-20 10:02:12','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Post 1 Lorem Ipsum','','inherit','closed','closed','','22-revision-v1','','','2020-12-20 11:02:12','2020-12-20 10:02:12','',22,'http://wordpress-101-alessandro-castellani.lab/22-revision-v1/',0,'revision','',0),
	(42,1,'2020-12-20 11:02:34','2020-12-20 10:02:34','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Recipe','','inherit','closed','closed','','24-revision-v1','','','2020-12-20 11:02:34','2020-12-20 10:02:34','',24,'http://wordpress-101-alessandro-castellani.lab/24-revision-v1/',0,'revision','',0),
	(43,1,'2020-12-20 11:03:10','2020-12-20 10:03:10','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Poem','','inherit','closed','closed','','26-revision-v1','','','2020-12-20 11:03:10','2020-12-20 10:03:10','',26,'http://wordpress-101-alessandro-castellani.lab/26-revision-v1/',0,'revision','',0),
	(44,1,'2020-12-20 11:04:54','2020-12-20 10:04:54','<div class=\"quoteDetails\">\r\n<div class=\"quoteText\">“There is something to be learned from a rainstorm. When meeting with a sudden shower, you try not to get wet and run quickly along the road. But doing such things as passing under the eaves of houses, you still get wet. When you are resolved from the beginning, you will not be perplexed, though you will still get the same soaking. This understanding extends to everything.”\r\n― <span class=\"authorOrTitle\"> Tsunetomo Yamamoto, </span> <span id=\"quote_book_link_283747\"> <a class=\"authorOrTitle\" href=\"https://www.goodreads.com/work/quotes/275268\">The Hagakure: A code to the way of samurai</a></span></div>\r\n<div></div>\r\n<div><a href=\"https://www.goodreads.com/work/quotes/275268\" target=\"_blank\" rel=\"noopener\">Source</a></div>\r\n</div>','Poem','','inherit','closed','closed','','26-revision-v1','','','2020-12-20 11:04:54','2020-12-20 10:04:54','',26,'http://wordpress-101-alessandro-castellani.lab/26-revision-v1/',0,'revision','',0),
	(45,1,'2020-12-20 11:06:54','2020-12-20 10:06:54','<table class=\"ingredients table-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 Dose/n</td>\r\n<td class=\"td-right\" width=\"66%\">Pfirsich(e)</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">½ Flasche</td>\r\n<td class=\"td-right\" width=\"66%\">Malibu Coconut Rum</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 ½ Liter</td>\r\n<td class=\"td-right\" width=\"66%\">Maracujasaft</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 Flasche</td>\r\n<td class=\"td-right\" width=\"66%\">Sekt, trocken</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2>Zubereitung</h2>\r\n<small class=\"ds-recipe-meta rds-recipe-meta\"><i class=\"material-icons\"></i> Arbeitszeit ca. 15 Minuten <i class=\"material-icons\"></i> Ruhezeit ca. 1 Tag <i class=\"material-icons\"></i> Gesamtzeit ca. 1 Tag 15 Minuten </small>\r\n<div class=\"ds-box\">Am Abend davor die Pfirsiche klein schneiden und mit etwas von der Flüssigkeit aus der Dose in den Malibu einlegen. Anschließend 24 Stunden kaltstellen.\r\n\r\nAm nächsten Tag mit dem gekühlten Sekt und Maracujasaft auffüllen und genießen.</div>','Recipe Malibu Peach Beach','','inherit','closed','closed','','24-revision-v1','','','2020-12-20 11:06:54','2020-12-20 10:06:54','',24,'http://wordpress-101-alessandro-castellani.lab/24-revision-v1/',0,'revision','',0),
	(46,1,'2020-12-20 11:35:47','2020-12-20 10:35:47','','malibu-peach-beach','','inherit','','closed','','malibu-peach-beach','','','2020-12-20 11:35:47','2020-12-20 10:35:47','',24,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/malibu-peach-beach.jpg',0,'attachment','image/jpeg',0),
	(47,1,'2020-12-20 11:35:52','2020-12-20 10:35:52','<table class=\"ingredients table-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 Dose/n</td>\r\n<td class=\"td-right\" width=\"66%\">Pfirsich(e)</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">½ Flasche</td>\r\n<td class=\"td-right\" width=\"66%\">Malibu Coconut Rum</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 ½ Liter</td>\r\n<td class=\"td-right\" width=\"66%\">Maracujasaft</td>\r\n</tr>\r\n<tr>\r\n<td class=\"td-left \" width=\"33%\">1 Flasche</td>\r\n<td class=\"td-right\" width=\"66%\">Sekt, trocken</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2>Zubereitung</h2>\r\n<small class=\"ds-recipe-meta rds-recipe-meta\"><i class=\"material-icons\"></i> Arbeitszeit ca. 15 Minuten <i class=\"material-icons\"></i> Ruhezeit ca. 1 Tag <i class=\"material-icons\"></i> Gesamtzeit ca. 1 Tag 15 Minuten </small>\r\n<div class=\"ds-box\">\r\n\r\nAm Abend davor die Pfirsiche klein schneiden und mit etwas von der Flüssigkeit aus der Dose in den Malibu einlegen. Anschließend 24 Stunden kaltstellen.\r\n\r\nAm nächsten Tag mit dem gekühlten Sekt und Maracujasaft auffüllen und genießen.\r\n\r\n</div>','Recipe Malibu Peach Beach','','inherit','closed','closed','','24-revision-v1','','','2020-12-20 11:35:52','2020-12-20 10:35:52','',24,'http://wordpress-101-alessandro-castellani.lab/24-revision-v1/',0,'revision','',0),
	(48,1,'2020-12-20 11:49:25','2020-12-20 10:49:25','<img class=\"alignnone size-medium wp-image-49\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/abstract-2-300x218.jpg\" alt=\"\" width=\"300\" height=\"218\" /> <img class=\"alignnone size-medium wp-image-50\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/adwords-300x199.jpg\" alt=\"\" width=\"300\" height=\"199\" /> <img class=\"alignnone size-medium wp-image-51\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/architecture-2-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" /> <img class=\"alignnone size-medium wp-image-52\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/architecture-3-300x225.jpg\" alt=\"\" width=\"300\" height=\"225\" /> <img class=\"alignnone size-medium wp-image-53\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art-2-300x179.jpg\" alt=\"\" width=\"300\" height=\"179\" /> <img class=\"alignnone size-medium wp-image-54\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" />','Gallery','','draft','closed','open','','gallery','','','2020-12-23 17:27:38','2020-12-23 16:27:38','',0,'http://wordpress-101-alessandro-castellani.lab/?p=48',0,'post','',0),
	(49,1,'2020-12-20 11:47:38','2020-12-20 10:47:38','','abstract-2','','inherit','','closed','','abstract-2','','','2020-12-20 11:47:38','2020-12-20 10:47:38','',48,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/abstract-2.jpg',0,'attachment','image/jpeg',0),
	(50,1,'2020-12-20 11:47:39','2020-12-20 10:47:39','','adwords','','inherit','','closed','','adwords','','','2020-12-20 11:47:39','2020-12-20 10:47:39','',48,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/adwords.jpg',0,'attachment','image/jpeg',0),
	(51,1,'2020-12-20 11:47:39','2020-12-20 10:47:39','','architecture-2','','inherit','','closed','','architecture-2','','','2020-12-20 11:47:39','2020-12-20 10:47:39','',48,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/architecture-2.jpg',0,'attachment','image/jpeg',0),
	(52,1,'2020-12-20 11:47:41','2020-12-20 10:47:41','','architecture-3','','inherit','','closed','','architecture-3','','','2020-12-20 11:47:41','2020-12-20 10:47:41','',48,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/architecture-3.jpg',0,'attachment','image/jpeg',0),
	(53,1,'2020-12-20 11:47:41','2020-12-20 10:47:41','','art-2','','inherit','','closed','','art-2','','','2020-12-20 11:47:41','2020-12-20 10:47:41','',48,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art-2.jpg',0,'attachment','image/jpeg',0),
	(54,1,'2020-12-20 11:47:42','2020-12-20 10:47:42','','art','','inherit','','closed','','art','','','2020-12-20 11:47:42','2020-12-20 10:47:42','',48,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art.jpg',0,'attachment','image/jpeg',0),
	(55,1,'2020-12-20 11:49:25','2020-12-20 10:49:25','<img class=\"alignnone size-medium wp-image-49\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/abstract-2-300x218.jpg\" alt=\"\" width=\"300\" height=\"218\" /> <img class=\"alignnone size-medium wp-image-50\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/adwords-300x199.jpg\" alt=\"\" width=\"300\" height=\"199\" /> <img class=\"alignnone size-medium wp-image-51\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/architecture-2-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" /> <img class=\"alignnone size-medium wp-image-52\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/architecture-3-300x225.jpg\" alt=\"\" width=\"300\" height=\"225\" /> <img class=\"alignnone size-medium wp-image-53\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art-2-300x179.jpg\" alt=\"\" width=\"300\" height=\"179\" /> <img class=\"alignnone size-medium wp-image-54\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/art-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" />','Gallery','','inherit','closed','closed','','48-revision-v1','','','2020-12-20 11:49:25','2020-12-20 10:49:25','',48,'http://wordpress-101-alessandro-castellani.lab/48-revision-v1/',0,'revision','',0),
	(56,1,'2020-12-20 11:55:11','2020-12-20 10:55:11','<a href=\"https://i.redd.it/8rderit6h4w11.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-57\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/zelda-2-map-300x204.png\" alt=\"\" width=\"300\" height=\"204\" /></a>\r\n\r\n<a href=\"https://i.redd.it/8rderit6h4w11.png\" target=\"_blank\" rel=\"noopener\">Link</a>','Link','','draft','closed','open','','link','','','2020-12-23 17:27:44','2020-12-23 16:27:44','',0,'http://wordpress-101-alessandro-castellani.lab/?p=56',0,'post','',0),
	(57,1,'2020-12-20 11:54:05','2020-12-20 10:54:05','','zelda-2-map','','inherit','','closed','','zelda-2-map','','','2020-12-20 11:54:05','2020-12-20 10:54:05','',56,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/zelda-2-map.png',0,'attachment','image/png',0),
	(58,1,'2020-12-20 11:55:11','2020-12-20 10:55:11','<a href=\"https://i.redd.it/8rderit6h4w11.png\" target=\"_blank\" rel=\"noopener\"><img class=\"alignnone size-medium wp-image-57\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/zelda-2-map-300x204.png\" alt=\"\" width=\"300\" height=\"204\" /></a>\r\n\r\n<a href=\"https://i.redd.it/8rderit6h4w11.png\" target=\"_blank\" rel=\"noopener\">Link</a>','Link','','inherit','closed','closed','','56-revision-v1','','','2020-12-20 11:55:11','2020-12-20 10:55:11','',56,'http://wordpress-101-alessandro-castellani.lab/56-revision-v1/',0,'revision','',0),
	(59,1,'2020-12-20 11:57:56','2020-12-20 10:57:56','<img class=\"alignnone size-medium wp-image-60\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/Master-Roshi-Quotes-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\r\n<blockquote>“You will not go in there with hopes of winning the tournament the first time you compete. To do so would be arrogant! And arrogance is for fools, not warriors! So you will enter the tournament with the sole purpose of improving your fighting skills.” – Master Roshi</blockquote>\r\n<a href=\"https://animemotivation.com/dragon-ball-z-quotes/\" target=\"_blank\" rel=\"noopener\">Source</a>','Quote','','draft','closed','open','','quote','','','2020-12-23 17:27:49','2020-12-23 16:27:49','',0,'http://wordpress-101-alessandro-castellani.lab/?p=59',0,'post','',0),
	(60,1,'2020-12-20 11:57:48','2020-12-20 10:57:48','','Master-Roshi-Quotes','','inherit','','closed','','master-roshi-quotes','','','2020-12-20 11:57:48','2020-12-20 10:57:48','',59,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/Master-Roshi-Quotes.jpg',0,'attachment','image/jpeg',0),
	(61,1,'2020-12-20 11:57:56','2020-12-20 10:57:56','<img class=\"alignnone size-medium wp-image-60\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/Master-Roshi-Quotes-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\r\n<blockquote>“You will not go in there with hopes of winning the tournament the first time you compete. To do so would be arrogant! And arrogance is for fools, not warriors! So you will enter the tournament with the sole purpose of improving your fighting skills.” – Master Roshi</blockquote>\r\n<a href=\"https://animemotivation.com/dragon-ball-z-quotes/\" target=\"_blank\" rel=\"noopener\">Source</a>','Quote','','inherit','closed','closed','','59-revision-v1','','','2020-12-20 11:57:56','2020-12-20 10:57:56','',59,'http://wordpress-101-alessandro-castellani.lab/59-revision-v1/',0,'revision','',0),
	(62,1,'2020-12-20 11:58:35','2020-12-20 10:58:35','Status: <em>Work in progress</em>.','Status','','draft','closed','open','','status','','','2020-12-23 17:27:55','2020-12-23 16:27:55','',0,'http://wordpress-101-alessandro-castellani.lab/?p=62',0,'post','',0),
	(63,1,'2020-12-20 11:58:35','2020-12-20 10:58:35','Status: <em>Work in progress</em>.','','','inherit','closed','closed','','62-revision-v1','','','2020-12-20 11:58:35','2020-12-20 10:58:35','',62,'http://wordpress-101-alessandro-castellani.lab/62-revision-v1/',0,'revision','',0),
	(64,1,'2020-12-20 11:58:43','2020-12-20 10:58:43','Status: <em>Work in progress</em>.','Status','','inherit','closed','closed','','62-revision-v1','','','2020-12-20 11:58:43','2020-12-20 10:58:43','',62,'http://wordpress-101-alessandro-castellani.lab/62-revision-v1/',0,'revision','',0),
	(65,1,'2020-12-20 11:59:49','2020-12-20 10:59:49','<iframe src=\"https://www.youtube.com/embed/ut5b0gSpV1w\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>','Video','','draft','closed','open','','video','','','2020-12-23 17:28:02','2020-12-23 16:28:02','',0,'http://wordpress-101-alessandro-castellani.lab/?p=65',0,'post','',0),
	(66,1,'2020-12-20 11:59:49','2020-12-20 10:59:49','<iframe src=\"https://www.youtube.com/embed/ut5b0gSpV1w\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>','Video','','inherit','closed','closed','','65-revision-v1','','','2020-12-20 11:59:49','2020-12-20 10:59:49','',65,'http://wordpress-101-alessandro-castellani.lab/65-revision-v1/',0,'revision','',0),
	(67,1,'2020-12-20 12:00:36','2020-12-20 11:00:36','[audio mp3=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/track006.mp3\"][/audio]','Audio','','draft','closed','open','','audio','','','2020-12-23 17:28:07','2020-12-23 16:28:07','',0,'http://wordpress-101-alessandro-castellani.lab/?p=67',0,'post','',0),
	(68,1,'2020-12-20 12:00:27','2020-12-20 11:00:27','\"_track006\" aus Tomb Raider 2 OST von Core Design Ltd.','_track006','','inherit','closed','closed','','_track006','','','2020-12-20 12:00:30','2020-12-20 11:00:30','',67,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/track006.mp3',0,'attachment','audio/mpeg',0),
	(69,1,'2020-12-20 12:00:36','2020-12-20 11:00:36','[audio mp3=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/track006.mp3\"][/audio]','Audio','','inherit','closed','closed','','67-revision-v1','','','2020-12-20 12:00:36','2020-12-20 11:00:36','',67,'http://wordpress-101-alessandro-castellani.lab/67-revision-v1/',0,'revision','',0),
	(70,1,'2020-12-20 12:01:54','2020-12-20 11:01:54','<img class=\"alignnone size-full wp-image-71\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/steam-chat.jpg\" alt=\"\" width=\"263\" height=\"192\" />','Chat','','draft','closed','open','','chat','','','2020-12-23 17:28:12','2020-12-23 16:28:12','',0,'http://wordpress-101-alessandro-castellani.lab/?p=70',0,'post','',0),
	(71,1,'2020-12-20 12:01:48','2020-12-20 11:01:48','','steam-chat','','inherit','','closed','','steam-chat','','','2020-12-20 12:01:48','2020-12-20 11:01:48','',70,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/steam-chat.jpg',0,'attachment','image/jpeg',0),
	(72,1,'2020-12-20 12:01:54','2020-12-20 11:01:54','<img class=\"alignnone size-full wp-image-71\" src=\"http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/steam-chat.jpg\" alt=\"\" width=\"263\" height=\"192\" />','Chat','','inherit','closed','closed','','70-revision-v1','','','2020-12-20 12:01:54','2020-12-20 11:01:54','',70,'http://wordpress-101-alessandro-castellani.lab/70-revision-v1/',0,'revision','',0),
	(73,1,'2020-12-21 09:46:11','2020-12-21 08:46:11','This is the Home page.\r\n\r\nPickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.','Home','','inherit','closed','closed','','8-revision-v1','','','2020-12-21 09:46:11','2020-12-21 08:46:11','',8,'http://wordpress-101-alessandro-castellani.lab/8-revision-v1/',0,'revision','',0),
	(74,1,'2020-12-21 14:08:55','2020-12-21 13:08:55','','wordpress-logo','','inherit','','closed','','wordpress-logo','','','2020-12-21 14:08:55','2020-12-21 13:08:55','',8,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/wordpress-logo.png',0,'attachment','image/png',0),
	(75,1,'2020-12-22 08:08:51','2020-12-22 07:08:51','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.\n\n<?php sprintf( the_ID()); ?>  ','About Me','','inherit','closed','closed','','10-autosave-v1','','','2020-12-22 08:08:51','2020-12-22 07:08:51','',10,'http://wordpress-101-alessandro-castellani.lab/10-autosave-v1/',0,'revision','',0),
	(77,1,'2020-12-22 08:09:20','2020-12-22 07:09:20','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.\r\n\r\n<div><?php sprintf( the_ID()); ?>  </div>','About Me','','inherit','closed','closed','','10-revision-v1','','','2020-12-22 08:09:20','2020-12-22 07:09:20','',10,'http://wordpress-101-alessandro-castellani.lab/10-revision-v1/',0,'revision','',0),
	(78,1,'2020-12-22 08:31:34','2020-12-22 07:31:34','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.\r\n\r\n','About Me','','inherit','closed','closed','','10-revision-v1','','','2020-12-22 08:31:34','2020-12-22 07:31:34','',10,'http://wordpress-101-alessandro-castellani.lab/10-revision-v1/',0,'revision','',0),
	(79,1,'2020-12-22 10:05:03','2020-12-22 09:05:03','Rockets are awesome\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Rocket Post','','publish','closed','open','','rocket-post','','','2020-12-22 10:08:37','2020-12-22 09:08:37','',0,'http://wordpress-101-alessandro-castellani.lab/?p=79',0,'post','',0),
	(80,1,'2020-12-22 10:04:47','2020-12-22 09:04:47','','lightbulb','','inherit','','closed','','lightbulb','','','2020-12-22 10:04:47','2020-12-22 09:04:47','',79,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/lightbulb.jpg',0,'attachment','image/jpeg',0),
	(81,1,'2020-12-22 10:04:47','2020-12-22 09:04:47','','rocket','','inherit','','closed','','rocket','','','2020-12-22 10:04:47','2020-12-22 09:04:47','',79,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/rocket.jpg',0,'attachment','image/jpeg',0),
	(82,1,'2020-12-22 10:04:47','2020-12-22 09:04:47','','umbrella','','inherit','','closed','','umbrella','','','2020-12-22 10:04:47','2020-12-22 09:04:47','',79,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/umbrella.jpg',0,'attachment','image/jpeg',0),
	(83,1,'2020-12-22 10:05:03','2020-12-22 09:05:03','Rockets are awesome\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Rocket Post','','inherit','closed','closed','','79-revision-v1','','','2020-12-22 10:05:03','2020-12-22 09:05:03','',79,'http://wordpress-101-alessandro-castellani.lab/79-revision-v1/',0,'revision','',0),
	(84,1,'2020-12-22 10:05:35','2020-12-22 09:05:35','Not Resident Evil\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Umbrella Post','','publish','open','open','','umbrella-post','','','2020-12-25 08:31:54','2020-12-25 07:31:54','',0,'http://wordpress-101-alessandro-castellani.lab/?p=84',0,'post','',0),
	(85,1,'2020-12-22 10:05:35','2020-12-22 09:05:35','Not Resident Evil\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Umbrella Post','','inherit','closed','closed','','84-revision-v1','','','2020-12-22 10:05:35','2020-12-22 09:05:35','',84,'http://wordpress-101-alessandro-castellani.lab/84-revision-v1/',0,'revision','',0),
	(86,1,'2020-12-22 10:06:04','2020-12-22 09:06:04','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post','','publish','closed','open','','lightbulb-post','','','2020-12-22 10:08:19','2020-12-22 09:08:19','',0,'http://wordpress-101-alessandro-castellani.lab/?p=86',0,'post','',0),
	(87,1,'2020-12-22 10:06:04','2020-12-22 09:06:04','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post','','inherit','closed','closed','','86-revision-v1','','','2020-12-22 10:06:04','2020-12-22 09:06:04','',86,'http://wordpress-101-alessandro-castellani.lab/86-revision-v1/',0,'revision','',0),
	(88,1,'2020-12-22 16:02:07','2020-12-22 15:02:07','This is a random post.','Random Post','','draft','closed','open','','tutorial-post','','','2020-12-23 16:11:31','2020-12-23 15:11:31','',0,'http://wordpress-101-alessandro-castellani.lab/?p=88',0,'post','',0),
	(89,1,'2020-12-22 16:02:07','2020-12-22 15:02:07','This is a tutorial post.','Tutorial Post','','inherit','closed','closed','','88-revision-v1','','','2020-12-22 16:02:07','2020-12-22 15:02:07','',88,'http://wordpress-101-alessandro-castellani.lab/88-revision-v1/',0,'revision','',0),
	(90,1,'2020-12-22 16:08:34','2020-12-22 15:08:34','This is a random post.','Random Post','','inherit','closed','closed','','88-revision-v1','','','2020-12-22 16:08:34','2020-12-22 15:08:34','',88,'http://wordpress-101-alessandro-castellani.lab/88-revision-v1/',0,'revision','',0),
	(91,1,'2020-12-22 16:30:41','2020-12-22 15:30:41','This is a tutorial post.','Another Tutorials Post','','trash','closed','open','','another-tutorials-post__trashed','','','2020-12-22 16:56:25','2020-12-22 15:56:25','',0,'http://wordpress-101-alessandro-castellani.lab/?p=91',0,'post','',0),
	(92,1,'2020-12-22 16:30:41','2020-12-22 15:30:41','This is a tutorial post.','Another Tutorials Post','','inherit','closed','closed','','91-revision-v1','','','2020-12-22 16:30:41','2020-12-22 15:30:41','',91,'http://wordpress-101-alessandro-castellani.lab/91-revision-v1/',0,'revision','',0),
	(93,1,'2020-12-23 14:18:07','2020-12-23 13:18:07','','Home','','inherit','closed','closed','','8-revision-v1','','','2020-12-23 14:18:07','2020-12-23 13:18:07','',8,'http://wordpress-101-alessandro-castellani.lab/8-revision-v1/',0,'revision','',0),
	(94,1,'2020-12-25 10:08:13','2020-12-25 09:08:13','Hipster ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','HIPSTER IPSUM','','publish','closed','open','','hipster-ipsum','','','2020-12-25 10:08:13','2020-12-25 09:08:13','',0,'http://wordpress-101-alessandro-castellani.lab/?p=94',0,'post','',0),
	(95,1,'2020-12-25 10:08:13','2020-12-25 09:08:13','Hipster ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','HIPSTER IPSUM','','inherit','closed','closed','','94-revision-v1','','','2020-12-25 10:08:13','2020-12-25 09:08:13','',94,'http://wordpress-101-alessandro-castellani.lab/94-revision-v1/',0,'revision','',0),
	(96,1,'2020-12-25 10:11:31','2020-12-25 09:11:31','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Random Blog Post','','publish','open','open','','random-blog-post','','','2020-12-25 10:43:23','2020-12-25 09:43:23','',0,'http://wordpress-101-alessandro-castellani.lab/?p=96',0,'post','',0),
	(97,1,'2020-12-25 10:11:23','2020-12-25 09:11:23','','sc','','inherit','','closed','','sc','','','2020-12-25 10:11:23','2020-12-25 09:11:23','',96,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/sc.jpg',0,'attachment','image/jpeg',0),
	(98,1,'2020-12-25 10:11:31','2020-12-25 09:11:31','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Random Blog Post','','inherit','closed','closed','','96-revision-v1','','','2020-12-25 10:11:31','2020-12-25 09:11:31','',96,'http://wordpress-101-alessandro-castellani.lab/96-revision-v1/',0,'revision','',0),
	(99,1,'2020-12-25 10:12:15','2020-12-25 09:12:15','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Last Blog Post','','publish','closed','open','','last-blog-post','','','2020-12-25 10:49:45','2020-12-25 09:49:45','',0,'http://wordpress-101-alessandro-castellani.lab/?p=99',0,'post','',0),
	(100,1,'2020-12-25 10:12:10','2020-12-25 09:12:10','','mk23','','inherit','','closed','','mk23','','','2020-12-25 10:12:10','2020-12-25 09:12:10','',99,'http://wordpress-101-alessandro-castellani.lab/wp-content/uploads/mk23.png',0,'attachment','image/png',0),
	(101,1,'2020-12-25 10:12:15','2020-12-25 09:12:15','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Last Blog Post','','inherit','closed','closed','','99-revision-v1','','','2020-12-25 10:12:15','2020-12-25 09:12:15','',99,'http://wordpress-101-alessandro-castellani.lab/99-revision-v1/',0,'revision','',0),
	(102,1,'2020-12-25 10:47:26','2020-12-25 09:47:26','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post copy','','draft','closed','open','','lightbulb-post-2','','','2020-12-25 10:52:03','2020-12-25 09:52:03','',0,'http://wordpress-101-alessandro-castellani.lab/?p=102',0,'post','',0),
	(103,1,'2020-12-25 10:47:34','2020-12-25 09:47:34','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post','','inherit','closed','closed','','102-revision-v1','','','2020-12-25 10:47:34','2020-12-25 09:47:34','',102,'http://wordpress-101-alessandro-castellani.lab/102-revision-v1/',0,'revision','',0),
	(104,1,'2020-12-25 10:48:00','2020-12-25 09:48:00','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post copy 2','','draft','closed','open','','lightbulb-post-copy-2','','','2020-12-25 10:51:56','2020-12-25 09:51:56','',0,'http://wordpress-101-alessandro-castellani.lab/?p=104',0,'post','',0),
	(105,1,'2020-12-25 10:48:18','2020-12-25 09:48:18','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post copy 2','','inherit','closed','closed','','104-revision-v1','','','2020-12-25 10:48:18','2020-12-25 09:48:18','',104,'http://wordpress-101-alessandro-castellani.lab/104-revision-v1/',0,'revision','',0),
	(106,1,'2020-12-25 10:48:24','2020-12-25 09:48:24','Illuminates\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Lightbulb Post copy','','inherit','closed','closed','','102-revision-v1','','','2020-12-25 10:48:24','2020-12-25 09:48:24','',102,'http://wordpress-101-alessandro-castellani.lab/102-revision-v1/',0,'revision','',0),
	(107,1,'2020-12-26 09:23:10','2020-12-26 08:23:10','','Search','','publish','closed','closed','','search','','','2020-12-28 09:12:48','2020-12-28 08:12:48','',0,'http://wordpress-101-alessandro-castellani.lab/?p=107',6,'nav_menu_item','',0),
	(108,1,'2020-12-26 09:55:21','2020-12-26 08:55:21','Pickled wolf sed biodiesel YOLO VHS velit +1. Voluptate humblebrag brunch nulla, yr hashtag next level YOLO tacos organic swag craft beer pug. Meditation skateboard brunch ethical knausgaard, paleo ullamco. Kinfolk wayfarers esse pok pok coloring book slow-carb butcher try-hard. Offal exercitation banjo umami, cupidatat master cleanse adipisicing.\r\n\r\n&nbsp;\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat sem at mauris suscipit porta. Cras metus velit, elementum sed pellentesque a, pharetra eu eros. Etiam facilisis placerat euismod. Nam faucibus neque arcu, quis accumsan leo tincidunt varius. In vel diam enim. Sed id ultrices ligula. Maecenas at urna arcu. Sed quis nulla sapien. Nam felis mauris, tincidunt at convallis id, tempor molestie libero. Quisque viverra sollicitudin nisl sit amet hendrerit. Etiam sit amet arcu sem. Morbi eu nibh condimentum, interdum est quis, tempor nisi. Vivamus convallis erat in pharetra elementum. Phasellus metus neque, commodo vitae venenatis sed, pellentesque non purus. Pellentesque egestas convallis suscipit. Ut luctus, leo quis porta vulputate, purus purus pellentesque ex, id consequat mi nisl quis eros. Integer ornare libero quis risus fermentum consequat. Mauris pharetra odio sagittis, vulputate magna at, lobortis nulla. Proin efficitur, nisi vel finibus elementum, orci sem volutpat eros, eget ultrices velit mi sagittis massa. Vestibulum sagittis ullamcorper cursus. Ut turpis dolor, tempor ut tellus et, sodales ultricies elit. Ut pharetra tristique est ac dictum. Integer ac consectetur purus, vehicula efficitur urna. Donec ultrices accumsan ipsum vitae faucibus. Quisque malesuada ex nisi, a bibendum erat commodo in. Pellentesque suscipit varius gravida. Nam scelerisque est sit amet laoreet pharetra. Vivamus quis ligula sed lacus mattis mollis. Vivamus facilisis orci rutrum nulla porta dignissim. Fusce fermentum id nibh laoreet volutpat. Suspendisse venenatis, risus sed euismod finibus, risus arcu fringilla augue, nec vulputate felis nisl et enim. In ornare, massa a cursus cursus, nisl mi ornare mauris, nec porttitor risus erat ut odio. Integer malesuada hendrerit purus ullamcorper molestie. Fusce imperdiet orci vitae purus suscipit rutrum. Nunc ac metus ac mi commodo sagittis at in leo. Vestibulum est lorem, consequat vel dictum non, imperdiet eu felis. Vivamus molestie sapien id quam rutrum, eget hendrerit eros finibus. Morbi in justo at felis blandit fringilla at faucibus purus. Donec ac aliquet purus, vitae tincidunt nulla. Curabitur ultricies auctor quam tincidunt molestie. Quisque tristique metus nunc, in pretium lectus dictum at. Nullam scelerisque placerat dui, maximus commodo augue mollis quis. Nulla gravida ex sed lectus consectetur laoreet et vel ex. Proin pretium libero non leo mattis, ullamcorper scelerisque diam hendrerit. Quisque gravida, dui a porttitor interdum, velit quam pulvinar arcu, eu blandit sapien velit ut lacus. Vestibulum congue eleifend odio, in aliquam leo sodales sed. Nam eu lacinia ipsum. Aenean sagittis nisl eu sodales lacinia. Duis nulla ante, suscipit a nibh sit amet, blandit luctus elit. Etiam a pellentesque lectus. Morbi pulvinar dolor a mi volutpat, sit amet finibus est mattis. Nunc egestas feugiat odio sed mollis. Vestibulum accumsan, velit id facilisis fermentum, nisi risus faucibus nulla, non condimentum mauris tortor eu dui. In urna ante, consectetur ac nunc in, hendrerit elementum lacus. Pellentesque pharetra felis sit amet erat pellentesque malesuada. Sed vel placerat lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque sit amet bibendum diam, sit amet convallis mi. Sed tristique leo sit amet urna lacinia ullamcorper sit amet non sem. Aliquam erat volutpat. Etiam accumsan sit amet odio sit amet posuere. Sed lobortis auctor est a mollis. Duis leo leo, condimentum finibus magna et, tempus tincidunt nisi. Praesent quis molestie elit. Mauris ante orci, luctus et lectus nec, vestibulum suscipit sem. Duis consequat felis id est pellentesque, sit amet vulputate dui commodo. Vestibulum efficitur, elit eu mattis faucibus, turpis nisl lacinia sapien, nec mattis ante massa non felis. Nulla at gravida orci, nec blandit nisl. Duis sed erat a tortor accumsan scelerisque. Donec congue quam vel urna consequat, id sagittis ante pretium. Vivamus posuere, arcu nec tincidunt eleifend, ligula metus convallis odio, nec pulvinar dui leo at tortor. Suspendisse potenti. Sed a euismod mi. Maecenas suscipit luctus enim, ut facilisis elit venenatis ut. Aenean ullamcorper at erat a dictum. Donec vel nulla elit. Aliquam pharetra massa elit, sed maximus lectus mollis ut. Phasellus a ligula et neque fringilla efficitur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam euismod lobortis enim et condimentum. Suspendisse pharetra.','About Me','','inherit','closed','closed','','10-revision-v1','','','2020-12-26 09:55:21','2020-12-26 08:55:21','',10,'http://wordpress-101-alessandro-castellani.lab/10-revision-v1/',0,'revision','',0),
	(112,1,'2020-12-28 08:16:23','2020-12-28 07:16:23','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Krita new Logo','','publish','closed','closed','','krita-new-logo','','','2020-12-28 14:12:08','2020-12-28 13:12:08','',0,'http://wordpress-101-alessandro-castellani.lab/?post_type=portfolio&#038;p=112',0,'portfolio','',0),
	(113,1,'2020-12-28 08:16:23','2020-12-28 07:16:23','','Logo for Business A','','inherit','closed','closed','','112-revision-v1','','','2020-12-28 08:16:23','2020-12-28 07:16:23','',112,'http://wordpress-101-alessandro-castellani.lab/112-revision-v1/',0,'revision','',0),
	(114,1,'2020-12-28 08:24:28','2020-12-28 07:24:28','','Krita new Logo','','inherit','closed','closed','','112-revision-v1','','','2020-12-28 08:24:28','2020-12-28 07:24:28','',112,'http://wordpress-101-alessandro-castellani.lab/112-revision-v1/',0,'revision','',0),
	(115,1,'2020-12-28 08:25:09','2020-12-28 07:25:09','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Nerd Continuity','','publish','closed','closed','','nerd-continuity','','','2020-12-28 14:11:56','2020-12-28 13:11:56','',0,'http://wordpress-101-alessandro-castellani.lab/?post_type=portfolio&#038;p=115',0,'portfolio','',0),
	(116,1,'2020-12-28 08:25:09','2020-12-28 07:25:09','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Nerd Continuity','','inherit','closed','closed','','115-revision-v1','','','2020-12-28 08:25:09','2020-12-28 07:25:09','',115,'http://wordpress-101-alessandro-castellani.lab/115-revision-v1/',0,'revision','',0),
	(117,1,'2020-12-28 08:25:31','2020-12-28 07:25:31','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Developer\'s life','','publish','closed','closed','','developers-life','','','2020-12-28 14:29:38','2020-12-28 13:29:38','',0,'http://wordpress-101-alessandro-castellani.lab/?post_type=portfolio&#038;p=117',0,'portfolio','',0),
	(118,1,'2020-12-28 08:25:31','2020-12-28 07:25:31','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Developer\'s life','','inherit','closed','closed','','117-revision-v1','','','2020-12-28 08:25:31','2020-12-28 07:25:31','',117,'http://wordpress-101-alessandro-castellani.lab/117-revision-v1/',0,'revision','',0),
	(119,1,'2020-12-28 08:28:07','2020-12-28 07:28:07','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem.','Krita new Logo','','inherit','closed','closed','','112-revision-v1','','','2020-12-28 08:28:07','2020-12-28 07:28:07','',112,'http://wordpress-101-alessandro-castellani.lab/112-revision-v1/',0,'revision','',0),
	(121,1,'2020-12-28 09:11:35','2020-12-28 08:11:35','','Work','','publish','closed','closed','','work','','','2020-12-28 10:02:40','2020-12-28 09:02:40','',0,'http://wordpress-101-alessandro-castellani.lab/?page_id=121',25,'page','',0),
	(122,1,'2020-12-28 09:11:35','2020-12-28 08:11:35','','Work','','inherit','closed','closed','','121-revision-v1','','','2020-12-28 09:11:35','2020-12-28 08:11:35','',121,'http://wordpress-101-alessandro-castellani.lab/121-revision-v1/',0,'revision','',0),
	(123,1,'2020-12-28 09:12:12','2020-12-28 08:12:12',' ','','','publish','closed','closed','','123','','','2020-12-28 09:12:48','2020-12-28 08:12:48','',0,'http://wordpress-101-alessandro-castellani.lab/?p=123',3,'nav_menu_item','',0),
	(124,1,'2020-12-28 10:02:21','2020-12-28 09:02:21','test','Work','','inherit','closed','closed','','121-revision-v1','','','2020-12-28 10:02:21','2020-12-28 09:02:21','',121,'http://wordpress-101-alessandro-castellani.lab/121-revision-v1/',0,'revision','',0),
	(125,1,'2020-12-28 10:02:40','2020-12-28 09:02:40','','Work','','inherit','closed','closed','','121-revision-v1','','','2020-12-28 10:02:40','2020-12-28 09:02:40','',121,'http://wordpress-101-alessandro-castellani.lab/121-revision-v1/',0,'revision','',0);

/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_term_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_term_relationships`;

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`)
VALUES
	(1,1,0),
	(15,2,0),
	(16,2,0),
	(17,2,0),
	(18,3,0),
	(19,3,0),
	(22,13,0),
	(24,5,0),
	(24,13,0),
	(26,1,0),
	(26,4,0),
	(31,2,0),
	(48,1,0),
	(48,6,0),
	(56,1,0),
	(56,7,0),
	(59,1,0),
	(59,8,0),
	(62,1,0),
	(62,9,0),
	(65,1,0),
	(65,10,0),
	(67,1,0),
	(67,11,0),
	(70,1,0),
	(70,12,0),
	(79,15,0),
	(84,14,0),
	(84,17,0),
	(86,13,0),
	(88,15,0),
	(88,16,0),
	(91,13,0),
	(94,16,0),
	(96,13,0),
	(99,15,0),
	(102,13,0),
	(104,13,0),
	(107,2,0),
	(112,24,0),
	(115,22,0),
	(117,18,0),
	(117,23,0),
	(117,24,0),
	(117,25,0),
	(117,26,0),
	(117,27,0),
	(123,2,0);

/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_term_taxonomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_term_taxonomy`;

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`)
VALUES
	(1,1,'category','',0,1),
	(2,2,'nav_menu','',0,6),
	(3,3,'nav_menu','',0,2),
	(4,4,'post_format','',0,1),
	(5,5,'post_format','',0,1),
	(6,6,'post_format','',0,0),
	(7,7,'post_format','',0,0),
	(8,8,'post_format','',0,0),
	(9,9,'post_format','',0,0),
	(10,10,'post_format','',0,0),
	(11,11,'post_format','',0,0),
	(12,12,'post_format','',0,0),
	(13,13,'category','Category Tutorial Description (WP Backend)',0,4),
	(14,14,'category','',0,1),
	(15,15,'category','',0,2),
	(16,16,'category','',0,1),
	(17,17,'post_tag','',0,1),
	(18,18,'type','',0,1),
	(19,19,'type','',0,0),
	(20,20,'type','',0,0),
	(21,21,'type','',18,0),
	(22,22,'field','',0,1),
	(23,23,'field','',0,1),
	(24,24,'field','',23,2),
	(25,25,'software','',0,1),
	(26,26,'software','',0,1),
	(27,27,'software','',0,1);

/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_termmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_termmeta`;

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Export von Tabelle wp_terms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_terms`;

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`)
VALUES
	(1,'Allgemein','allgemein',0),
	(2,'Main Navigation','main-navigation',0),
	(3,'Footer Navigation','footer-navigation',0),
	(4,'post-format-aside','post-format-aside',0),
	(5,'post-format-image','post-format-image',0),
	(6,'post-format-gallery','post-format-gallery',0),
	(7,'post-format-link','post-format-link',0),
	(8,'post-format-quote','post-format-quote',0),
	(9,'post-format-status','post-format-status',0),
	(10,'post-format-video','post-format-video',0),
	(11,'post-format-audio','post-format-audio',0),
	(12,'post-format-chat','post-format-chat',0),
	(13,'Tutorials','tutorials',0),
	(14,'Reviews','reviews',0),
	(15,'News','news',0),
	(16,'Random','random',0),
	(17,'umbrella corp.','umbrella-corp',0),
	(18,'Design','design',0),
	(19,'Developement','developement',0),
	(20,'Print','print',0),
	(21,'Logo Design','logo-design',0),
	(22,'Developement','developement',0),
	(23,'Design','design',0),
	(24,'Logo Design','logo-design',0),
	(25,'Illustrator','illustrator',0),
	(26,'Photoshop','photoshop',0),
	(27,'Krita','krita',0);

/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_usermeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_usermeta`;

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`)
VALUES
	(1,1,'nickname','bigbossy'),
	(2,1,'first_name',''),
	(3,1,'last_name',''),
	(4,1,'description',''),
	(5,1,'rich_editing','true'),
	(6,1,'syntax_highlighting','true'),
	(7,1,'comment_shortcuts','false'),
	(8,1,'admin_color','fresh'),
	(9,1,'use_ssl','0'),
	(10,1,'show_admin_bar_front','true'),
	(11,1,'locale',''),
	(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),
	(13,1,'wp_user_level','10'),
	(14,1,'dismissed_wp_pointers',''),
	(15,1,'show_welcome_panel','1'),
	(16,1,'session_tokens','a:1:{s:64:\"cd0da8e0dec4ab5f8afd1e141076011a42023297065ccb80021abbee7cca7be7\";a:4:{s:10:\"expiration\";i:1610282623;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:82:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 Firefox/84.0\";s:5:\"login\";i:1609073023;}}'),
	(17,1,'wp_dashboard_quick_press_last_post_id','4'),
	(18,1,'community-events-location','a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
	(19,1,'managenav-menuscolumnshidden','a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
	(20,1,'metaboxhidden_nav-menus','a:1:{i:0;s:12:\"add-post_tag\";}'),
	(21,1,'nav_menu_recently_edited','2'),
	(22,1,'closedpostboxes_page','a:0:{}'),
	(23,1,'metaboxhidden_page','a:6:{i:0;s:12:\"revisionsdiv\";i:1;s:10:\"postcustom\";i:2;s:16:\"commentstatusdiv\";i:3;s:11:\"commentsdiv\";i:4;s:7:\"slugdiv\";i:5;s:9:\"authordiv\";}'),
	(24,1,'wp_user-settings','libraryContent=browse&editor=tinymce&advImgDetails=show'),
	(25,1,'wp_user-settings-time','1608622295'),
	(26,1,'closedpostboxes_post','a:0:{}'),
	(27,1,'metaboxhidden_post','a:5:{i:0;s:11:\"postexcerpt\";i:1;s:13:\"trackbacksdiv\";i:2;s:10:\"postcustom\";i:3;s:7:\"slugdiv\";i:4;s:9:\"authordiv\";}');

/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle wp_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_users`;

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`)
VALUES
	(1,'bigbossy','$P$B4Jm7tdq7NtcL7BXZ9FyCjeWcdV/BO.','bigbossy','reichenbach.s@gmail.com','http://wordpress-101-alessandro-castellani.lab','2020-12-13 09:06:37','',0,'bigbossy');

/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
